﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web;
using System.Xml;

namespace AutomatizacionBase
{
    /// <summary>
    /// Esta clase se encarga de generar los html para enviar por mail y enviar los mismos utilizando una cuenta de gmail
    /// </summary>
    public class EmailManager
    {
        public static string email = "encodesatestingreportes@gmail.com";
        public static string pass = "reqcczomrjjsdndm";
        public static string recipiente;

        /// <summary>
        /// Este metodo genera un html cuando solo hay 1 prueba
        /// En el caso de ser fallida le agrega la imagen sacada al final de la prueba
        /// </summary>
        /// <returns>El documento html, como XmlDocument</returns>
        static XmlDocument GenerarHTMLIndividual(string titulo,
            string fecha_hora,
            ResultadoPrueba prueba)
        {
            XmlDocument doc = new XmlDocument();
            if (prueba.resultado == ResultadoPrueba.Resultado.Aprobada)
            {
                doc.Load(AppDomain.CurrentDomain.BaseDirectory + @"HTML\Email_Template_Individual_Sucess.html");
            }
			else if (prueba.resultado == ResultadoPrueba.Resultado.Fallada)
			{
                doc.Load(AppDomain.CurrentDomain.BaseDirectory + @"HTML\Email_Template_Individual_Failure.html");
                doc.InnerXml = doc.InnerXml.Replace("{detalles_salida}", HttpUtility.HtmlEncode(prueba.detalles_salida)
                .Replace(Environment.NewLine, "<br />"))
                .Replace("{imagen_prueba}", @"cid:" + HttpUtility.HtmlEncode(prueba.imagen_prueba));
            }
			else if (prueba.resultado == ResultadoPrueba.Resultado.Alerta)
			{
                doc.Load(AppDomain.CurrentDomain.BaseDirectory + @"HTML\Email_Template_Individual_Alert.html");
                doc.InnerXml = doc.InnerXml.Replace("{detalles_salida}", HttpUtility.HtmlEncode(prueba.detalles_salida)
                .Replace(Environment.NewLine, "<br />"))
                .Replace("{imagen_prueba}", @"cid:" + HttpUtility.HtmlEncode(prueba.imagen_prueba));
            }

            doc.InnerXml = doc.InnerXml.Replace("{titulo}", HttpUtility.HtmlEncode(titulo))
                .Replace("{fecha_hora}", HttpUtility.HtmlEncode(fecha_hora))
                .Replace("{nombre_prueba}", HttpUtility.HtmlEncode(prueba.nombre_prueba))
                .Replace("{duracion}", HttpUtility.HtmlEncode(prueba.duracion));

            return doc;
        }


        /// <summary>
        /// Este metodo genera un html cuando solo hay mas de 1 prueba
        /// Le agrega la imagen obtenida a las pruebas fallidas
        /// </summary>
        /// <returns>El documento html, como XmlDocument</returns>
        static XmlDocument GenerarHTMLMultiple(
            string titulo,
            string fecha_hora,
            string grados_porcentaje_pasadas,
            string grados_porcentaje_falladas,
            string grados_porcentaje_alerta,
            string pruebas_pasadas,
            string pruebas_falladas,
            string pruebas_alerta,
            string pruebas_totales,
            string duracion_total,
            Dictionary<Navegador, ResultadoPrueba[]> pruebas)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(AppDomain.CurrentDomain.BaseDirectory + @"HTML\Email_Template_Multiple.html");

            doc.InnerXml = doc.InnerXml.Replace("{titulo}", HttpUtility.HtmlEncode(titulo))
                .Replace("{fecha_hora}", HttpUtility.HtmlEncode(fecha_hora))
                .Replace("{porcentaje_pasadas}", HttpUtility.HtmlEncode(grados_porcentaje_pasadas))
                .Replace("{porcentaje_falladas}", HttpUtility.HtmlEncode(grados_porcentaje_falladas))
                .Replace("{porcentaje_alerta}", HttpUtility.HtmlEncode(grados_porcentaje_alerta))
                .Replace("{pruebas_pasadas}", HttpUtility.HtmlEncode(pruebas_pasadas))
                .Replace("{pruebas_falladas}", HttpUtility.HtmlEncode(pruebas_falladas))
                .Replace("{pruebas_alerta}", HttpUtility.HtmlEncode(pruebas_alerta))
                .Replace("{pruebas_totales}", HttpUtility.HtmlEncode(pruebas_totales))
                .Replace("{duracion_total}", HttpUtility.HtmlEncode(duracion_total));

            XmlNode nodoPruebaPasada = doc.DocumentElement.SelectSingleNode("//div[@id='prueba_pasada']");
            XmlNode nodoPruebaFallada = doc.DocumentElement.SelectSingleNode("//div[@id='prueba_fallada']");
            XmlNode nodoPruebaAlerta = doc.DocumentElement.SelectSingleNode("//div[@id='prueba_alerta']");

            XmlNode nodoDetallesImagen = doc.DocumentElement.SelectSingleNode("//div[@id='detalles_imagen']");
            XmlNode nodoDetallesAprobada = doc.DocumentElement.SelectSingleNode("//div[@id='detalles_aprobada']");

            nodoPruebaPasada.ParentNode.RemoveChild(nodoPruebaPasada);
            nodoPruebaFallada.ParentNode.RemoveChild(nodoPruebaFallada);
            nodoPruebaAlerta.ParentNode.RemoveChild(nodoPruebaAlerta);
            nodoDetallesImagen.ParentNode.RemoveChild(nodoDetallesImagen);
            nodoDetallesAprobada.ParentNode.RemoveChild(nodoDetallesAprobada);

            nodoPruebaPasada.Attributes.RemoveNamedItem("id");
            nodoPruebaFallada.Attributes.RemoveNamedItem("id");
            nodoPruebaAlerta.Attributes.RemoveNamedItem("id");
            nodoDetallesImagen.Attributes.RemoveNamedItem("id");
            nodoDetallesAprobada.Attributes.RemoveNamedItem("id");

            XmlNode nodoListaPruebasChrome = doc.DocumentElement.SelectSingleNode("//div[@id='lista_pruebas_chrome']");
            XmlNode nodoListaPruebasFirefox = doc.DocumentElement.SelectSingleNode("//div[@id='lista_pruebas_firefox']");
            XmlNode nodoListaPruebasIE = doc.DocumentElement.SelectSingleNode("//div[@id='lista_pruebas_ie']");


            if (pruebas.ContainsKey(Navegador.Chrome))
            {
                if (pruebas[Navegador.Chrome].Length == 0)
                {
                    XmlNode node = doc.DocumentElement.SelectSingleNode("//div[@id='chrome_icon']");
                    node.ParentNode.RemoveChild(node);
                }
                for (int i = 0; i < pruebas[Navegador.Chrome].Length; i++)
                {
                    AgregarNodo(nodoListaPruebasChrome, nodoPruebaPasada, nodoPruebaFallada, nodoPruebaAlerta, nodoDetallesImagen, nodoDetallesAprobada, pruebas[Navegador.Chrome][i]);
                }
            }
            else
            {
                XmlNode node = doc.DocumentElement.SelectSingleNode("//div[@id='chrome_icon']");
                node.ParentNode.RemoveChild(node);
            }

            if (pruebas.ContainsKey(Navegador.Firefox))
            {
                if (pruebas[Navegador.Firefox].Length == 0)
                {
                    XmlNode node = doc.DocumentElement.SelectSingleNode("//div[@id='firefox_icon']");
                    node.ParentNode.RemoveChild(node);
                }
                for (int i = 0; i < pruebas[Navegador.Firefox].Length; i++)
                {
                    AgregarNodo(nodoListaPruebasFirefox, nodoPruebaPasada, nodoPruebaFallada, nodoPruebaAlerta, nodoDetallesImagen, nodoDetallesAprobada, pruebas[Navegador.Firefox][i]);
                }
            }
            else
            {
                XmlNode node = doc.DocumentElement.SelectSingleNode("//div[@id='firefox_icon']");
                node.ParentNode.RemoveChild(node);
            }

            if (pruebas.ContainsKey(Navegador.IE))
            {
                if (pruebas[Navegador.IE].Length == 0)
                {
                    XmlNode node = doc.DocumentElement.SelectSingleNode("//div[@id='ie_icon']");
                    node.ParentNode.RemoveChild(node);
                }
                for (int i = 0; i < pruebas[Navegador.IE].Length; i++)
                {
                    AgregarNodo(nodoListaPruebasIE, nodoPruebaPasada, nodoPruebaFallada, nodoPruebaAlerta, nodoDetallesImagen, nodoDetallesAprobada, pruebas[Navegador.IE][i]);
                }
			}
			else
            {
                XmlNode node = doc.DocumentElement.SelectSingleNode("//div[@id='ie_icon']");
                node.ParentNode.RemoveChild(node);
            }

            return doc;
        }

        static void AgregarNodo(XmlNode nodoListaPruebas, XmlNode nodoPruebaPasada, XmlNode nodoPruebaFallada, XmlNode nodoPruebaAlerta, XmlNode nodoContenido, XmlNode nodoAprobada, ResultadoPrueba prueba)
        {
            XmlNode nodo = null;
			switch (prueba.resultado)
			{
				case ResultadoPrueba.Resultado.Aprobada:
                    nodo = nodoPruebaPasada.Clone();
                    break;
				case ResultadoPrueba.Resultado.Fallada:
                    nodo = nodoPruebaFallada.Clone();
                    break;
				case ResultadoPrueba.Resultado.Alerta:
                    nodo = nodoPruebaAlerta.Clone();
                    break;
			}

            nodo.InnerXml = nodo.InnerXml.Replace("{nombre_prueba}", HttpUtility.HtmlEncode(prueba.nombre_prueba))
                .Replace("{duracion}", HttpUtility.HtmlEncode(prueba.duracion)).Replace("{descripcion}", HttpUtility.HtmlEncode(prueba.descripcion));

            XmlNode content = nodo.SelectSingleNode("//div[@id='contenido']");
            content.Attributes.RemoveNamedItem("id");

            for (int i = 0; i < prueba.resultados.Count; i++)
            {
                if (prueba.resultados[i].resultado == ResultadoPrueba.Resultado.Aprobada)
                {
                    XmlNode sucess = nodoAprobada.Clone();
                    content.AppendChild(sucess);
                    continue;
                }
                XmlNode item_content = nodoContenido.Clone();

                item_content.InnerXml = item_content.InnerXml.
                Replace("{detalles_salida}", HttpUtility.HtmlEncode(prueba.resultados[i].detalles_salida).Replace(Environment.NewLine, "<br />")).
                Replace("{imagen_prueba}", @"cid:" + HttpUtility.HtmlEncode(prueba.resultados[i].imagen_prueba));

				if (prueba.resultados[i].resultado == ResultadoPrueba.Resultado.Alerta)
				{
                    item_content.Attributes.GetNamedItem("class").Value += " alert";
                }
				else if (prueba.resultados[i].resultado == ResultadoPrueba.Resultado.Fallada)
                {
                    item_content.Attributes.GetNamedItem("class").Value += " failure";
                }

                content.AppendChild(item_content);
            }

            nodoListaPruebas.AppendChild(nodo);
        }

        /// <summary>
        /// Este metodo se engarga de recopilar la informacion relevante para la generacion del html y generarlo
        /// </summary>
        public static XmlDocument GenerarReporteEmail(Dictionary<Navegador, ResultadoPrueba[]> listadoDeResultados, string titulo, string duration)
        {
            string fecha = $"{DateTime.Now.Day}-{DateTime.Now.Month}-{DateTime.Now.Year} {DateTime.Now.Hour}:{DateTime.Now.Minute}";

            int cantPruebasTotales = 0;
            foreach (Navegador nav in listadoDeResultados.Keys)
            {
                for (int i = 0; i < listadoDeResultados[nav].Length; i++)
                {
                    cantPruebasTotales++;
                }
            }
            XmlDocument doc;
            if (cantPruebasTotales == 1)
            {
                doc = GenerarHTMLIndividual(
                    titulo,
                    fecha,
                    listadoDeResultados[new List<Navegador>(listadoDeResultados.Keys)[0]][0]);
            }
            else
            {
                int aprobadas = 0;
                int falladas = 0;
                int alerta = 0;
                foreach (Navegador nav in listadoDeResultados.Keys)
				{
                    for (int i = 0; i < listadoDeResultados[nav].Length; i++)
                    {
						switch (listadoDeResultados[nav][i].resultado)
						{
							case ResultadoPrueba.Resultado.Aprobada: aprobadas++; break;
							case ResultadoPrueba.Resultado.Fallada:  falladas++;  break;
							case ResultadoPrueba.Resultado.Alerta:   alerta++;    break;
							default: break;
						}
                    }
                }

                float indiceAprobadas = (float)aprobadas / (float)cantPruebasTotales;
                float indiceFalladas = (float)falladas / (float)cantPruebasTotales;
                float indiceAlerta = (float)alerta / (float)cantPruebasTotales;
                int grados_aprobadas = (int)(indiceAprobadas * 100);
                int grados_falladas = (int)(indiceFalladas * 100);
                int grados_alerta = (int)(indiceAlerta * 100);

                doc = GenerarHTMLMultiple(
                    titulo,
                    fecha,
                    grados_aprobadas.ToString(),
                    grados_falladas.ToString(),
                    grados_alerta.ToString(),
                    aprobadas.ToString(),
                    falladas.ToString(),
                    alerta.ToString(),
                    listadoDeResultados.Count.ToString(),
                    duration,
                    listadoDeResultados);
            }

            return doc;
        }


        /// <summary>
        /// Este metodo se encarga de obtener todas las imagenes a enviar en el mail, agregarlas y enviarlo
        /// </summary>
        public static void EnviarReporteMail(Dictionary<Navegador, ResultadoPrueba[]> listadoDeResultados, XmlDocument doc, string titulo)
        {
            List<string> attachments = new List<string>();
			foreach (Navegador item in listadoDeResultados.Keys)
			{
                ResultadoPrueba[] pruebas = listadoDeResultados[item];
                for (int j = 0; j < pruebas.Length; j++)
                {
                    if (pruebas[j].resultado == ResultadoPrueba.Resultado.Aprobada) continue;

                    attachments.Add(pruebas[j].imagen_prueba);
                }
            }
            string fecha = $"{DateTime.Now.Day}-{DateTime.Now.Month}-{DateTime.Now.Year} {DateTime.Now.Hour}:{DateTime.Now.Minute}";

            string subject = titulo + "_" + fecha;

            SendEmail(recipiente, subject, doc.OuterXml, attachments.ToArray());
        }

        /// <summary>
        /// Envia por email el texto dado
        /// </summary>
        static void SendEmail(string pToEmail, string pSubject, string pBody, string[] attachments)
        {
            try
            {
                AlternateView alternateView = AlternateView.CreateAlternateViewFromString(pBody, Encoding.UTF8, MediaTypeNames.Text.Html);

                for (int i = 0; i < attachments.Length; i++)
                {
                    LinkedResource image = new LinkedResource(AppDomain.CurrentDomain.BaseDirectory + @"Capturas\" + attachments[i], MediaTypeNames.Image.Jpeg);

                    image.ContentId = attachments[i];
                    image.ContentType.Name = image.ContentId;
                    image.ContentLink = new Uri("cid:" + image.ContentId);
                    image.ContentType.MediaType = MediaTypeNames.Image.Jpeg;
                    image.TransferEncoding = TransferEncoding.Base64;

                    alternateView.LinkedResources.Add(image);
                }


                MailMessage message = new MailMessage();
                message.From = new MailAddress(email, "Resultado Testing No Reply");
                message.Subject = pSubject;
                message.To.Add(pToEmail);
                message.IsBodyHtml = true;
                message.AlternateViews.Add(alternateView);

                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(email, pass)
                };

                //TODO: PAT, solucion1 de esta forma funciona
                ServicePointManager.ServerCertificateValidationCallback = delegate (object s,
                    System.Security.Cryptography.X509Certificates.X509Certificate certificate,
                    X509Chain chain, SslPolicyErrors sslPolicyErrors)
                { return true; };

                smtp.Send(message);
            }
            catch (SmtpException e)
            {
                Console.WriteLine("Ha ocurrido un error en el envío de mail: " + e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine("Ha ocurrido un error en el envío de mail: " + e.Message);
            }
        }
    }
}
