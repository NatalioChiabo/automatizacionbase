﻿using System;
using System.Collections.Generic;

namespace AutomatizacionBase
{
	public class Menu
	{
		public bool running = true;
		readonly bool numberInput = false;
		int selection = -1;
		string lableBefore;
		string lableAfter;
		public MenuLable[] items;
		MenuOption[] options;

		public Menu(string lableBefore, string lableAfter, MenuLable[] items = null, bool numberInput = false)
		{
			this.lableBefore = lableBefore;
			this.lableAfter = lableAfter;
			this.numberInput = numberInput;
			SetItems(items);
		}

		public void SetItems(MenuLable[] items)
		{
			if (items == null)
			{
				items = Array.Empty<MenuLable>();
			}
			this.items = items;
			List<MenuOption> ops = new List<MenuOption>();
			for (int i = 0; i < items.Length; i++)
			{
				if (items[i] is MenuOption)
				{
					ops.Add(items[i] as MenuOption);
				}
			}
			options = ops.ToArray();
			if (options.Length > 0)
			{
				selection = 0;
			}
		}

		public void DrawMenu(int x = 0, int y = 0)
		{
			Console.Clear();
			while (running)
			{
				WriteMenu(x, y);

				ConsoleKeyInfo info = Console.ReadKey(true);
				if (numberInput) CheckNumericInput(info);
				switch (info.Key)
				{
					case ConsoleKey.Enter:
						Enter();
						break;
					case ConsoleKey.UpArrow:
						Arriba();
						break;
					case ConsoleKey.DownArrow:
						Abajo();
						break;
					case ConsoleKey.LeftArrow:
						Izquierda();
						break;
					case ConsoleKey.RightArrow:
						Derecha();
						break;
					case ConsoleKey.Escape:
						StopMenu();
						break;
					default:
						CheckOptionalInput(info);
						break;
				}
			}
		}

		void Abajo()
		{
			selection++;
			if (selection == options.Length)
			{
				selection = 0;
			}
		}
		void Arriba()
		{

			selection--;
			if (selection == -1)
			{
				selection = options.Length - 1;
			}
		}

		void Izquierda()
		{
			if (selection == -1) return;
			options[selection].horizontal?.Invoke(false);
		}
		void Derecha()
		{
			if (selection == -1) return;
			options[selection].horizontal?.Invoke(true);
		}

		void Enter()
		{
			if (selection == -1) return;
			options[selection].accion?.Invoke();
			if (options[selection].stopMenu)
			{
				StopMenu();
			}
		}

		void CheckOptionalInput(ConsoleKeyInfo info)
		{
			char option = info.KeyChar;
			for (int i = 0; i < options.Length; i++)
			{
				MenuOption op = options[i];
				char[] optionalInput = op.OptionaInput;
				if (optionalInput == null)
				{
					continue;
				}
				for (int o = 0; o < optionalInput.Length; o++)
				{
					if (option == optionalInput[o])
					{
						selection = i;
						Enter();
					}
				}
			}
		}
		void CheckNumericInput(ConsoleKeyInfo info)
		{
			if (int.TryParse(info.KeyChar.ToString(), out int number))
			{
				if (number <= options.Length)
				{
					selection = number;
					Enter();
				}
			}
		}


		void WriteMenu(int x, int y)
		{
			Console.SetCursorPosition(x, y);
			Console.WriteLine(lableBefore);
			for (int i = 0; i < items.Length; i++)
			{
				if (items[i] is MenuOption)
				{
					MenuOption op = items[i] as MenuOption;

					Console.Write("	");
					if (op == options[selection])
					{
						Console.Write(">");
					}
					if (numberInput)
					{
						Console.Write($"{new List<MenuOption>(options).IndexOf(op)} - ");
					}
					Console.Write(op.lable);

					if (op.OptionaInput != null)
					{
						if (op.OptionaInput.Length == 1)
						{
							Console.Write($"[{op.OptionaInput[0]}]");
						}
						else
						{
							Console.Write("[");
							for (int o = 0; o < op.OptionaInput.Length; o++)
							{
								Console.Write(op.OptionaInput[o] + ", ");
							}
							Console.Write("]");
						}
					}
					Console.WriteLine("         ");
				}
				else
				{
					Console.WriteLine(items[i].lable);
				}
			}
			Console.WriteLine(lableAfter);
		}

		public void StopMenu()
		{
			running = false;
			Console.Clear();
		}

		public static void FormatearConsola()
		{
			Console.ResetColor();
			Console.Clear();
			Console.BackgroundColor = ConsoleColor.Magenta;
			Console.ForegroundColor = ConsoleColor.White;
		}
	}

	/// <summary>
	/// Un item dentro de un menu, no seleccionable, puedes usarlo para separar secciones de opciones
	/// </summary>
	public class MenuLable
	{
		public MenuLable(string lable)
		{
			this.lable = lable;
		}
		public string lable;
	}

	/// <summary>
	/// Un item dentro de un menu, seleccionable, puede tener una accion al presionar enter, o al usar las flechas izquierda y derecha
	/// </summary>
	public class MenuOption : MenuLable
	{
		public Option accion;
		public Horizontal horizontal;
		readonly char[] optionalInput;
		public bool stopMenu;
		public char[] OptionaInput { get { return optionalInput; } }

		/// <summary>
		/// Crea una nuevo opcion para un menu
		/// </summary>
		/// <param name="lable">El texto que se mostrara</param>
		/// <param name="accion">La accion a ejecutar al presionar ENTER con este item seleccionado</param>
		/// <param name="horizontal">La accion al ejecutar al presionar izquierda o derecha con este item seleccionado</param>
		/// <param name="optionalInput">Array de chars que presionar para automaticamente ejecutar la accion de este item sin que este seleccionado</param>
		/// <param name="stopMenu">Si es true al seleccionar esta opcion se ejecutara su accion y luego se saldra del menu</param>
		public MenuOption(string lable, Option accion, Horizontal horizontal = null, char[] optionalInput = null, bool stopMenu = false) : base(lable)
		{
			this.accion = accion;
			this.optionalInput = optionalInput;
			this.horizontal = horizontal;
			this.stopMenu = stopMenu;
		}

		public delegate void Option();
		public delegate void Horizontal(bool derecha);

	}

	public class SubMenu : MenuOption
	{
		Menu menu;
		public SubMenu(string lable, Menu menu, Horizontal horizontal = null, char[] optionalInput = null) : base(lable, null, horizontal, optionalInput)
		{
			this.menu = menu;
			this.accion = DrawSubMenu;
		}
		void DrawSubMenu()
		{
			menu.DrawMenu();
		}
	}
}
