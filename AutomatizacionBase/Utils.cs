﻿using OpenQA.Selenium;
using System;
using OpenQA.Selenium.Support.UI;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace AutomatizacionBase
{
    public static class Utils
    {
        //Los pongo aca para facilmente ajustar los tiempos sin ir uno por uno
        public static int shortSleep = 800;
        public static int midSleep = 1000;
        public static int longSleep = 1200;
        public static int extraLongSleep = 3000;

        public static IWebElement FindElement(this By by)
        {
            try
            {
                WaitFor(by, 10, 500);
                IWebElement element = Driver.driver.FindElement(by);
                return element;
            }
            catch (NotFoundException)
            {
                throw new PruebaFalladaException("No se encontro el elemento: " + by.ToString());
            }
        }

        /// <summary>
        /// Busca un elemento, le hace clic y lo retorna.
        /// </summary>
        /// <param name="by">El By que representa al elemento al cual se le quiere hacer click</param>
        /// <returns>El elemento encontrado</returns>
        /// <exception cref="OpenQA.Selenium.NoSuchElementException">Si el elemento no se encuentra</exception>
        public static IWebElement Click(this By by)
        {
            IWebElement to_click = FindElement(by);
            new WebDriverWait(Driver.driver, TimeSpan.FromSeconds(20)).Until(ExpectedConditions.ElementToBeClickable(by)).Click();
            //to_click.Click();
            return to_click;
        }

        private static string GenerateXPath(this IWebElement childElement)
        {
            string childTag = childElement.TagName;
            if (childTag.Equals("html"))
            {
                return "/html[1]";
            }
            IWebElement parentElement = childElement.FindElement(By.XPath(".."));
            var childrenElements = parentElement.FindElements(By.XPath("*"));
            int count = 0;
            for (int i = 0; i < childrenElements.Count; i++)
            {
                IWebElement childrenElement = childrenElements[i];
                string childrenElementTag = childrenElement.TagName;
                if (childTag.Equals(childrenElementTag))
                {
                    count++;
                }
                if (childElement.Equals(childrenElement))
                {
                    return GenerateXPath(parentElement) + "/" + childTag + "[" + count + "]";
                }
            }
            return "Could not find XPath";
        }

        public static void ClickJS(string xpath)
        {
            string javascript = $"document.evaluate( \"{xpath}\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue?.click()";
            IJavaScriptExecutor jsExecutor = (IJavaScriptExecutor)Driver.driver;
            jsExecutor.ExecuteScript(javascript);
        }

        public static void ClickJS(this IWebElement element)
        {
            ClickJS(element.GenerateXPath());
        }

        public static void ClickJS(this By by)
        {
            ClickJS(by.FindElement());
        }
        public static void ScrollIntoViewJS(this IWebElement element)
        {
            string javascript = "arguments[0].scrollIntoView(true);";
            IJavaScriptExecutor jsExecutor = (IJavaScriptExecutor)Driver.driver;
            jsExecutor.ExecuteScript(javascript, element);
        }
        public static void ScrollIntoViewJS(this By by)
        {
            ScrollIntoViewJS(by.FindElement());
        }

        /// <summary>
        /// Busca un elemento, le manda el texto y lo retorna.
        /// </summary>
        /// <param name="by">El By que representa al elemento al cual se le quiere enviar texto</param>
        /// <returns>El elemento encontrado</returns>
        /// <exception cref="OpenQA.Selenium.NoSuchElementException">Si el elemento no se encuentra</exception>
        public static IWebElement SendKeys( this By by, string keys)
        {
            IWebElement to_send = FindElement(by);
            try
            {
                to_send.SendKeys(keys);
            }
            catch (ElementNotInteractableException)
            {
                Thread.Sleep(longSleep);
                try
                {
                    to_send.SendKeys(keys);
                }
                catch (ElementNotInteractableException)
                {
                    throw;
                }
            }
            return to_send;

        }

        /// <summary>
        /// Busca un elemento, limpia el texto actual, le manda el texto y lo retorna.
        /// </summary>
        /// <param name="by">El By que representa al elemento al cual se le quiere enviar texto</param>
        /// <returns>El elemento encontrado</returns>
        /// <exception cref="OpenQA.Selenium.NoSuchElementException">Si el elemento no se encuentra</exception>
        public static IWebElement ClearSendKeys( this By by, string keys)
        {
            IWebElement to_send = new WebDriverWait(Driver.driver, TimeSpan.FromSeconds(30))
                .Until(ExpectedConditions.ElementToBeClickable(by));
            try
            {
                to_send.Clear();
                to_send.SendKeys(keys);
            }
            catch (ElementNotInteractableException)
            {
                Thread.Sleep(longSleep);
                try
                {
                    to_send.Clear();
                    to_send.SendKeys(keys);
                }
                catch (ElementNotInteractableException)
                {
                    throw;
                }
            }
            return to_send;
        }

        /// <summary>
        /// Busca un checkbox, le hace click si fuese necesario y lo retorna.
        /// </summary>
        /// <param name="by">El By que representa al checkbox al cual se quiere clickear</param>
        /// <returns>El elemento encontrado</returns>
        /// <exception cref="OpenQA.Selenium.NoSuchElementException">Si el elemento no se encuentra</exception>
        public static IWebElement Checkbox(this By by, bool value)
        {
            IWebElement checkbox = Driver.driver.FindElement(by);
            if (checkbox.Selected != value)
            {
                checkbox.Click();
            }
            return checkbox;
        }

        /// <summary>
        /// Busca un checkbox y comprueba si esta selecionado.
        /// </summary>
        /// <param name="by">El By que representa al checkbox al cual se le quiere comprobar el estado</param>
        /// <returns>El elemento encontrado</returns>
        /// <exception cref="OpenQA.Selenium.NoSuchElementException">Si el elemento no se encuentra</exception>
        public static bool IsChecked(this By by)
        {
            return FindElement(by).Selected;
        }

        /// <summary>
        /// Busca un elemento y retorna el valor del atributo 'value'.
        /// </summary>
        /// <param name="by">El By que representa al elemento al cual se le quiere obtener los hijos</param>
        /// <returns>El elemento encontrado</returns>
        /// <exception cref="OpenQA.Selenium.NoSuchElementException">Si el elemento no se encuentra</exception>
        public static string GetValue(this By by)
        {
            return FindElement(by).GetAttribute("value");
        }

        /// <summary>
        /// Busca un elemento y retorna el valor del atributo.
        /// </summary>
        /// <param name="by">El By que representa al elemento al cual se le quiere obtener los hijos</param>
        /// <returns>El elemento encontrado</returns>
        /// <exception cref="OpenQA.Selenium.NoSuchElementException">Si el elemento no se encuentra</exception>
        public static string GetAttribute(this By by, string attribute)
        {
            return FindElement(by).GetAttribute(attribute);
        }

        /// <summary>
        /// Busca un elemento y retorna el texto que emcompasa.
        /// </summary>
        /// <param name="by">El By que representa al elemento al cual se le quiere obtener los hijos</param>
        /// <returns>El elemento encontrado</returns>
        /// <exception cref="OpenQA.Selenium.NoSuchElementException">Si el elemento no se encuentra</exception>
        public static string GetText(this By by)
        {
            return FindElement(by).Text;
        }

        /// <summary>
        /// Devuelve los hijos del elemento representado por el By
        /// </summary>
        /// <param name="by">El By que representa al elemento al cual se le quiere obtener los hijos</param>
        /// <returns>Retorna un array con todoslos hijos</returns>
        public static IWebElement[] GetChildren(this By by)
        {
            IWebElement parent = FindElement(by);
            return GetChildren(parent);
        }

        /// <summary>
        /// Devuelve los hijos del elemento parent
        /// </summary>
        /// <param name="parent">Elemento al cual se le quiere obtener los hijos</param>
        /// <returns>Retorna un array con todos los hijos</returns>
        public static IWebElement[] GetChildren(this IWebElement parent)
        {
            var coleccion = parent.FindElements(By.XPath("./child::*"));

            IWebElement[] children = new IWebElement[coleccion.Count];
            coleccion.CopyTo(children, 0);

            return children;
        }
        /// <summary>
        /// Devuelve los hijos del elemento parent
        /// </summary>
        /// <param name="parent">Elemento al cual se le quiere obtener los hijos</param>
        /// <returns>Retorna el hijo en ese indice</returns>
        public static IWebElement Child(this IWebElement parent, int index)
        {
            return parent.FindElement(By.XPath($"./*[{index}]"));
        }

        /// <summary>
        /// Devuelve el hijo al final de una larga cadena de hijos, cada int en indexes representa el indice del hijo del elemento anterior.
        /// Entonces si uno ingresa 0,1,2,3 se retornara el 4to hijo, del 3er hijo, del 2do hijo, del 1er hijo.
        /// </summary>
        /// <param name="parent">Elemento al cual se le quiere obtener los hijos</param>
        /// <param name="indexes">La cadena de hijos</param>
        /// <returns>Retorna el hijo al final de esa cadena</returns>
        public static IWebElement ChildChain(this IWebElement parent, params int[] indexes)
        {
            StringBuilder sb = new StringBuilder(".");
			for (int i = 0; i < indexes.Length; i++)
			{
                sb.Append($"/*[{indexes[i]}]");
			}

            return parent.FindElement(By.XPath(sb.ToString()));
        }

        /// <summary>
        /// Atajo para botones 'Confirmar'
        /// </summary>
        //public static void ConfirmarMensaje()
        //{
        //    WaitForApear(PageUtils.confirmar, PageUtils.confirmar);
        //}

        /// <summary>
        /// Atajo para botones 'Aceptar'
        /// </summary>
        //public static void AceptarNotificacion()
        //{
        //    WaitForApear(PageUtils.aceptar, PageUtils.aceptar);
        //}



        /// <summary>
        /// Busca el dropdown representado por el by y elige el item definido por el texto
        /// </summary>
        /// <param name="text">El texto usado para elegir el elemento deseado</param>
        /// <param name="by">Objeto de tipo By que representa el Dropdown</param>
        /// <param name="partialMatch">Si es falso el texto debera ser exacto, si no se realizara una comprobacion parcial</param>
        /// <returns>El SelectElement creado con el elemento encontrado</returns>
        /// <exception cref="OpenQA.Selenium.NoSuchElementException">Cuando no encuentra el elemento</exception>
        /// <exception cref="OpenQA.Selenium.Support.UI.UnexpectedTagNameException">Cuando el elemento no es del tipo apropiado</exception>
        public static SelectElement DropdownSelectText( this By by, string text, bool partialMatch = false)
        {
            IWebElement elem = FindElement(by);
            SelectElement select = new SelectElement(elem);
            try
            {
                select.SelectByText(text, partialMatch);
                return select;
            }
            catch (NoSuchElementException)
            {
                throw new PruebaFalladaException("No se encontro el item con el texto " + text);
            }
        }

        /// <summary>
        /// Busca el dropdown representado por el by y elige el item definido por el texto
        /// </summary>
        /// <param name="index">El indice del elemento deseado</param>
        /// <param name="by">Objeto de tipo By que representa el Dropdown</param>
        /// <returns>El SelectElement creado con el elemento encontrado</returns>
        /// <exception cref="OpenQA.Selenium.NoSuchElementException">Cuando no encuentra el elemento</exception>
        /// <exception cref="OpenQA.Selenium.Support.UI.UnexpectedTagNameException">Cuando el elemento no es del tipo apropiado</exception>
        public static SelectElement DropdownSelectIndex(this By by, int index)
        {
            IWebElement elem = FindElement(by);
            SelectElement select = new SelectElement(elem);
            
            try
            {
                select.SelectByIndex(index);
                return select;
            }
            catch (NoSuchElementException)
            {
                throw new PruebaFalladaException("No se encontro el item con el indice " + index);
            }
        }


        /// <summary>
        /// Atajo para Assert.AreEqual en el que se realiza dentro de un try y en caso de causar una excepcion cierra el WebDriver
        /// </summary>
        /// <param name="esperado">El objeto esperado</param>
        /// <param name="actual">El objeto obtenido</param>
        /// <param name="mensaje">El mensaje a mostrar</param>
        /// <exception cref="AssertFailedException">Cuando no se satisface la igualdad</exception>
        public static void TryAssertEqual(object esperado, object actual, string mensaje)
        {
            if (!actual.Equals(esperado))
            {
                throw new PruebaFalladaException(mensaje);
            }
        }

        /// <summary>
        /// Atajo para Assert.IsFalse en el que se realiza dentro de un try y en caso de causar una excepcion cierra el WebDriver
        /// </summary>
        /// <param name="condicion">La condicion a evaluar</param>
        /// <param name="mensaje">El mensaje a mostrar</param>
        /// <exception cref="AssertFailedException">Cuando la condicion es falsa</exception>
        public static void TryAssertFalse(bool condicion, string mensaje)
        {
            if (!condicion)
            {
                throw new PruebaFalladaException(mensaje);
            }
        }

        /// <summary>
        /// Comprueba si un elemento existe
        /// </summary>
        /// <param name="element">El By que representa al elemento al cual se le quiere hacer click</param>
        /// <returns>Un bool representando si el elemento existe o no</returns>
        public static bool ElementExists(this By element)
        {
            Driver.driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromMilliseconds(0);
            bool exists = Driver.driver.FindElements(element).Count != 0;
            Driver.driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromMilliseconds(3000);
            return exists;
        }

        /// <summary>
        /// Busca un elemento y los devuelve si existe, si no existe devuelve null
        /// </summary>
        /// <param name="element">El By que representa al elemento al cual se le quiere hacer click</param>
        /// <returns>El elemento buscado o null</returns>
        public static IWebElement ElementExistsGet(this By element)
        {
            Driver.driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromMilliseconds(0);
            IReadOnlyCollection<IWebElement> elems = Driver.driver.FindElements(element);
            Driver.driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromMilliseconds(3000);
            if (elems.Count != 0)
            {
                return elems.ElementAt(0);
            }
            return null;
        }

        /// <summary>
        /// Espera a que un elemento se cierre o a que otro aparezca. A mas intentos y menor delay se realizaran mas comprobaciones en el mismo tiempo
        /// </summary>
        /// <param name="toClose">El By que representa al elemento a cerrarse</param>
        /// <param name="toApear">El By que representa al elemento a aparecer</param>
        /// <param name="toClick">El By que representa al elemento a clickear si el elemento 'toApear' aparece</param>
        /// <param name="tries">Los intentos a realziar</param>
        /// <param name="sleep">El tiempo entre intentos</param>
        public static void WaitForCloseOrApear(By toClose, By toApear, By toClick, int tries = 30, int sleep = 200)
        {
            while (tries > 0)
            {
                if (ElementExists(toClose))
                {
                    break;
                }
                else
                {
                    if (ElementExists(toApear))
                    {
                        Thread.Sleep(sleep);
                        Click(toClick);
                        break;
                    }
                }
                Thread.Sleep(sleep);
                tries--;
            }
            Thread.Sleep(sleep);
        }

        /// <summary>
        /// Espera a que un elemento aparezca o a que otro se cierre. A mas intentos y menor delay se realizaran mas comprobaciones en el mismo tiempo
        /// </summary>
        /// <param name="toClose">El By que representa al elemento a cerrarse</param>
        /// <param name="toApear">El By que representa al elemento a aparecer</param>
        /// <param name="toClick">El By que representa al elemento a clickear si el elemento 'toClose' se cierra</param>
        /// <param name="tries">Los intentos a realziar</param>
        /// <param name="sleep">El tiempo entre intentos</param>
        public static void WaitForApearOrClose(By toClose, By toApear, By toClick, int tries = 30, int sleep = 200)
        {
            while (tries > 0)
            {
                if (ElementExists(toApear))
                {
                    break;
                }
                else
                {
                    if (ElementExists(toClose))
                    {
                        Thread.Sleep(sleep);
                        Click(toClick);
                        break;
                    }
                }
                Thread.Sleep(sleep);
                tries--;
            }
            Thread.Sleep(sleep);
        }

        /// <summary>
        /// Espera a que un elemento aparezca. A mas intentos y menor delay se realizaran mas comprobaciones en el mismo tiempo
        /// </summary>
        /// <param name="toApear">El By que representa al elemento a aparecer</param>
        /// <param name="toClick">El By que representa al elemento a clickear si el elemento 'toClose' se cierra</param>
        /// <param name="tries">Los intentos a realziar</param>
        /// <param name="sleep">El tiempo entre intentos</param>
        public static void WaitForApear(By toApear, By toClick, int tries = 30, int sleep = 200)
        {
            while (tries > 0)
            {
                if (ElementExists(toApear))
                {
                    Thread.Sleep(sleep);
                    Click(toClick);
                    break;
                }
                Thread.Sleep(sleep);
                tries--;
            }
            Thread.Sleep(sleep);
        }

        /// <summary>
        /// Espera a que un elemento aparezca. A mas intentos y menor delay se realizaran mas comprobaciones en el mismo tiempo
        /// </summary>
        /// <param name="toApear">El By que representa al elemento a aparecer</param>
        /// <param name="tries">Los intentos a realziar</param>
        /// <param name="sleep">El tiempo entre intentos</param>
        public static bool WaitFor(this By toApear, int tries = 30, int sleep = 200)
        {
            while (tries > 0)
            {
                if (ElementExists(toApear))
                {
                    return true;
                }
                Thread.Sleep(sleep);
                tries--;
            }
            return false;
        }

        /// <summary>
        /// Espera a que un elemento aparezca. A mas intentos y menor delay se realizaran mas comprobaciones en el mismo tiempo
        /// </summary>
        /// <param name="toClose">El By que representa al elemento a aparecer</param>
        /// <param name="toClick">El By que representa al elemento a clickear si el elemento 'toClose' se cierra</param>
        /// <param name="tries">Los intentos a realziar</param>
        /// <param name="sleep">El tiempo entre intentos</param>
        public static void WaitForClose(By toClose, By toClick, int tries = 30, int sleep = 200)
        {
            while (tries > 0)
            {
                if (ElementExists(toClose))
                {
                    Thread.Sleep(sleep);
					Click(toClick);
                    break;
                }
                Thread.Sleep(sleep);
                tries--;
            }
            Thread.Sleep(sleep);
        }

        /// <summary>
        /// Genera un Cuil basado en un dni generazo al azar
        /// </summary>
        public static string GenerarCuil()
        {
            Random r = new Random();
            int dni = r.Next(10000000, 99999999);
            string dni_s = dni.ToString();

            return CalcularCuil(dni_s);
        }

        /// <summary>
        /// Genera un Cuil basado en un dni generazo al azar
        /// </summary>
        public static string CalcularCuil( string dni )
        {
            int x = 2;
            int y = 0;
            int nx = 10;
            int ny = 0;
            int n1 = int.Parse(dni[0].ToString()) * 3;
            int n2 = int.Parse(dni[1].ToString()) * 2;
            int n3 = int.Parse(dni[2].ToString()) * 7;
            int n4 = int.Parse(dni[3].ToString()) * 6;
            int n5 = int.Parse(dni[4].ToString()) * 5;
            int n6 = int.Parse(dni[5].ToString()) * 4;
            int n7 = int.Parse(dni[6].ToString()) * 3;
            int n8 = int.Parse(dni[7].ToString()) * 2;

            int sum = nx + ny + n1 + n2 + n3 + n4 + n5 + n6 + n7 + n8;
            int z;
            float rest = (float)sum / 11f;
            if (rest == 0f)
            {
                z = 0;
            }
            else if (rest == 1)
            {
                z = 9;
                y = 3;
            }
            else
            {
                int temp = sum - ((int)Math.Truncate(rest) * 11);
                z = 11 - temp;
            }

            return x.ToString() + y.ToString() + dni + z.ToString();
        }

        /// <summary>
		/// Ejecuta el ultimo archivo descargado, tienes que cambiar el directorio al directorio donde estas descargando archivos
		/// </summary>
		public static Process StartLastDownloadedFile(string dir)
        {
            FileInfo[] files = new DirectoryInfo(dir).GetFiles();
            List<FileInfo> list = new List<FileInfo>(files);
            list.Sort((FileInfo a, FileInfo b) => { return b.LastWriteTime.CompareTo(a.LastWriteTime); });

            Process p = Process.Start(list[0].FullName);

            return p;
        }
    }

}
