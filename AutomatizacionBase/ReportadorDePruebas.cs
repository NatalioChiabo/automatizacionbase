﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Xml;

namespace AutomatizacionBase
{
    public enum Navegador { Chrome, Firefox, IE }

    /// <summary>
    /// Esta clase se encarga de correr las pruebas, realizar el diagnostico, generar el html y enviarlo por mail
    /// </summary>
    public static class ReportadorDePruebas
    {
        /// <summary>
        /// Esto es el listado de todas las pruebas
        /// Cada prueba tiene un nombre y una referencia al metodo de inicio.
        /// </summary>
        public static Prueba[] todas_las_pruebas = { };

        /// <summary>
        /// Esto es el listado de todas los navegadores para los cuales realizar un prueba
        /// usar muchos navegadores causara que las pruebas tarden mucho mas en terminar ya que cada prueba tiene que ejecutarse en cada navegador
        /// </summary>
        public static Navegador[] navegadores_a_usar = new Navegador[]
        {
            Navegador.Chrome
        };

        public static int threadLimit = 3;
        static int threadCount = 0;
        static int testsLeft;


        [ThreadStatic]
        public static Navegador navegador;

        /// <summary>
        /// La secuencia de pasos que tomo cada prueba, si la prueba falla, esta secuencia se enviara en el reporte
        /// para facilitar el diagnostico de error.
        /// </summary>
        [ThreadStatic]
        public static SecuenciaDePasos secuenciaDePasos;

        /// <summary>
        /// Correra las pruebas y generara el reporte sin ejecutarlo para que puedas mandarlo por mail
        /// </summary>
        /// <param name="threading">Ejecutar multiple pruebas en paralelo, puede causar problemas si se utilizan los mismos usuarios</param>
        public static void CorrerPruebas(string titulo, Prueba[] pruebas, bool threading = false, bool open_email = false)
        {
            BorrarImagenes();

            Dictionary<Navegador, ResultadoPrueba[]> listadoDeResultados;
            string duration;
            if (threading && pruebas.Length > 1)
            {
                listadoDeResultados = CorrerPruebasEnParalelo(pruebas, out duration);
            }
            else
            {
                listadoDeResultados = CorrerPruebasUnHilo(pruebas, out duration);
            }

            XmlDocument doc = EmailManager.GenerarReporteEmail(listadoDeResultados, titulo, duration);
            if (open_email)
            {
                doc.InnerXml = doc.InnerXml.Replace("cid:", @"..\Capturas\");
            }
            doc.Save(AppDomain.CurrentDomain.BaseDirectory + @"HTML\Email_Output.html");
			if (open_email)
			{
                System.Diagnostics.Process.Start(AppDomain.CurrentDomain.BaseDirectory + @"HTML\Email_Output.html");
            }
        }

        /// <summary>
        /// Este metodo corre todas las pruebas dadas.
        /// Si la prueba se completa sin error se llama el metodo PasarPrueba()
        /// Si durante la prueba ocurre algun error, si no se encontrase algun elemento web, o si una comprobacion fallase
        /// entonces se tirara la excepcion 'PruebaFalladaException' que sera atrapada por el try/catch y procedera a FallarPrueba()
        /// </summary>
        static Dictionary<Navegador, ResultadoPrueba[]> CorrerPruebasUnHilo(Prueba[] pruebas, out string duration)
        {
            Dictionary<Navegador, ResultadoPrueba[]> listadoDeResultados = new Dictionary<Navegador, ResultadoPrueba[]>();
            System.Diagnostics.Stopwatch cronometroGeneral = new System.Diagnostics.Stopwatch();
            cronometroGeneral.Start();

			foreach (Navegador nav in navegadores_a_usar)
			{
                listadoDeResultados.Add(nav, new ResultadoPrueba[pruebas.Length]);

                for (int i = 0; i < pruebas.Length; i++)
                {
                    listadoDeResultados[nav][i] = CorrerPruebaIndividual(pruebas[i], nav); ;
                }
            }

            cronometroGeneral.Stop();
            duration = cronometroGeneral.ElapsedMilliseconds.ToString();
            return listadoDeResultados;
        }

        /// <summary>
        /// Este metodo corre todas las pruebas dadas en paralelo.
        /// Si la prueba se completa sin error se llama el metodo PasarPrueba()
        /// Si durante la prueba ocurre algun error, si no se encontrase algun elemento web, o si una comprobacion fallase
        /// entonces se tirara la excepcion 'PruebaFalladaException' que sera atrapada por el try/catch y procedera a FallarPrueba()
        /// </summary>
        static Dictionary<Navegador, ResultadoPrueba[]> CorrerPruebasEnParalelo(Prueba[] pruebas, out string duration)
        {
            Dictionary<Navegador, ResultadoPrueba[]> listadoDeResultados = new Dictionary<Navegador, ResultadoPrueba[]>();
            System.Diagnostics.Stopwatch cronometroGeneral = new System.Diagnostics.Stopwatch();
            cronometroGeneral.Start();

            testsLeft = pruebas.Length * navegadores_a_usar.Length;
            foreach (Navegador nav in navegadores_a_usar)
            {
                listadoDeResultados.Add(nav, new ResultadoPrueba[pruebas.Length]);
                for (int i = 0; i < pruebas.Length; i++)
                {
                    while (threadCount >= threadLimit)
                    {
                        Thread.Sleep(2000);
                    }
                    threadCount++;

                    int cashedIndex = i;


                    Thread testThread = new Thread(() =>
                    {
                        listadoDeResultados[nav][cashedIndex] = CorrerPruebaIndividual(pruebas[cashedIndex], nav);
                        threadCount--;
                        testsLeft--;
                    });
                    testThread.Start();
                }
            }

            while (testsLeft > 0)
            {
                Thread.Sleep(500);
            }

            cronometroGeneral.Stop();
            duration = cronometroGeneral.ElapsedMilliseconds.ToString();
            return listadoDeResultados;
        }

        /// <summary>
        /// Este metodo corre la prueba indicada.
        /// </summary>
        static ResultadoPrueba CorrerPruebaIndividual(Prueba prueba, Navegador d)
        {
            navegador = d;
            secuenciaDePasos = new SecuenciaDePasos();
            ResultadoPrueba pruebaActual = new ResultadoPrueba(prueba.nombre_prueba, prueba.descripcion);

            System.Diagnostics.Stopwatch cronometroIndividual = new System.Diagnostics.Stopwatch();

            int totalTries = 3;

            for (int tries = 1; tries <= totalTries; tries++)
            {

                secuenciaDePasos.Clear();
                try
                {
                    cronometroIndividual.Start();

                    secuenciaDePasos.AbrirPaso(prueba.nombre_prueba);

                    switch (d)
                    {
                        case Navegador.Chrome:
                            Driver.IniciarChromeDriver();
                            break;
                        case Navegador.Firefox:
                            Driver.IniciarFireFoxDriver();
                            break;
                        case Navegador.IE:
                            Driver.IniciarIEDriver();
                            break;
                    }

                    prueba.metodo_prueba();

                    cronometroIndividual.Stop();


                    Driver.AlTerminarPrueba();

                    PasarPrueba(pruebaActual, cronometroIndividual.ElapsedMilliseconds.ToString());

                    Driver.driver?.Quit();
                    secuenciaDePasos.Clear();
                    Console.Clear();
                    break;
                }
                catch (PruebaFalladaException ex)
                {
                    cronometroIndividual.Stop();

                    Driver.AlTerminarPrueba();
                    FallarPrueba(pruebaActual, ex.Message, cronometroIndividual.ElapsedMilliseconds.ToString());

                    Driver.driver?.Quit();
                    secuenciaDePasos.Clear();
                    Console.Clear();
                    break;
                }
                catch (WebDriverException ex)
                {                    
                    cronometroIndividual.Stop();

                    Driver.AlTerminarPrueba();

                    if (tries < totalTries)
                    {
                        secuenciaDePasos.AgregarPaso("");
                        secuenciaDePasos.AgregarPaso("Reintentando despues de error de selenium: " + ex.Message);
                        secuenciaDePasos.AgregarPaso("");

                    }

                    AlertarPrueba(pruebaActual,
                        Environment.NewLine +
                        "Excepcion de selenium no esperada." +
                        Environment.NewLine +
                        Environment.NewLine +
                        ex.Message +
                        Environment.NewLine +
                        ex.StackTrace, cronometroIndividual.ElapsedMilliseconds.ToString());

                    Driver.driver?.Quit();
                    secuenciaDePasos.Clear();
                    Console.Clear();
                }
                catch (Exception ex)
                {
                    cronometroIndividual.Stop();

                    Driver.AlTerminarPrueba();

                    if (tries < totalTries)
                    {
                        secuenciaDePasos.AgregarPaso("");
                        secuenciaDePasos.AgregarPaso("Reintentando despues de error inesperado: " + ex.Message);
                        secuenciaDePasos.AgregarPaso("");
                    }

                    FallarPrueba(pruebaActual,
                        Environment.NewLine +
                        "Excepcion no esperada." +
                        Environment.NewLine +
                        Environment.NewLine +
                        ex.Message +
                        Environment.NewLine +
                        ex.StackTrace, cronometroIndividual.ElapsedMilliseconds.ToString());

                    Driver.driver?.Quit();
                    secuenciaDePasos.Clear();
                    Console.Clear();
                }

            }
            secuenciaDePasos.CerrarPaso(prueba.nombre_prueba);

            secuenciaDePasos.Clear();
            cronometroIndividual.Reset();

            Console.Clear();
            return pruebaActual;
        }


        /// <summary>
        /// Este metodo se ejecuta cuando una prueba falla
        /// Se encarga de recopilar la informacion relevante a la prueba (la secuencia de pasos, el mensaje de error, duracion, etc)
        /// Sacar una imagen del momento en el que fallo la prueba
        /// Cerrar el WebDriver
        /// Etc
        /// </summary>
        static void FallarPrueba(ResultadoPrueba prueba, string detalles_salida, string duracion)
        {
            DateTime now = DateTime.Now;
            string nombre = prueba.nombre_prueba.Replace(" ", "_");
            string imagen_prueba = nombre + $"_{now.Day}_{now.Month}_{now.Year}__{now.Hour}_{now.Minute}_{now.Second}.jpg";

            bool image = ImprimirImagen(imagen_prueba);

            string detalles = "";
            for (int i = 0; i < secuenciaDePasos.secuencia.Count; i++)
            {
                prueba.detalles_salida += secuenciaDePasos.secuencia[i] + Environment.NewLine;
                detalles += secuenciaDePasos.secuencia[i] + Environment.NewLine;
            }

            prueba.resultado = ResultadoPrueba.Resultado.Fallada;
            prueba.detalles_salida += detalles_salida;
            detalles += detalles_salida;
            prueba.imagen_prueba = imagen_prueba;
            prueba.duracion = duracion;

            prueba.AgregarResultado(ResultadoPrueba.Resultado.Fallada, duracion, detalles, imagen_prueba);
        }

        /// <summary>
        /// Este metodo se ejecuta cuando una prueba falla por un error fuera de el proyecto
        /// Se encarga de recopilar la informacion relevante a la prueba (la secuencia de pasos, el mensaje de error, duracion, etc)
        /// Sacar una imagen del momento en el que fallo la prueba
        /// Cerrar el WebDriver
        /// Etc
        /// </summary>
        static void AlertarPrueba(ResultadoPrueba prueba, string detalles_salida, string duracion)
        {
            DateTime now = DateTime.Now;
            string nombre = prueba.nombre_prueba.Replace(" ", "_");
            string imagen_prueba = nombre + $"_{now.Day}_{now.Month}_{now.Year}__{now.Hour}_{now.Minute}_{now.Second}.jpg";

            bool image = ImprimirImagen(imagen_prueba);
            string detalles = "";

            for (int i = 0; i < secuenciaDePasos.secuencia.Count; i++)
            {
                prueba.detalles_salida += secuenciaDePasos.secuencia[i] + Environment.NewLine;
                detalles += secuenciaDePasos.secuencia[i] + Environment.NewLine;
            }

            prueba.resultado = ResultadoPrueba.Resultado.Alerta;
            prueba.detalles_salida += detalles_salida;
            detalles += detalles_salida;
            prueba.imagen_prueba = imagen_prueba;
            prueba.duracion = duracion;

            prueba.AgregarResultado(ResultadoPrueba.Resultado.Alerta, duracion, detalles, imagen_prueba);
        }


        /// <summary>
        /// Este metodo se ejecuta cuando una prueba pasa
        /// Una prueba exitosa no necesita demasiada informacion, solo la duracion
        /// Cerrar el WebDriver
        /// </summary>
        static void PasarPrueba(ResultadoPrueba prueba, string duracion)
        {
            prueba.resultado = ResultadoPrueba.Resultado.Aprobada;
            prueba.duracion = duracion;

            prueba.AgregarResultado(ResultadoPrueba.Resultado.Aprobada, duracion, "", "");
        }


        /// <summary>
        /// Este metodo Imprime la imagen actual del WebDriver
        /// </summary>
        static bool ImprimirImagen(string titulo)
        {
            try
            {
                Thread.Sleep(2000);

                if (Driver.driver != null)
                {
                    Screenshot imagen = ((ITakesScreenshot)Driver.driver).GetScreenshot();
                    imagen.SaveAsFile(AppDomain.CurrentDomain.BaseDirectory + @"Capturas\" + titulo);

                    Console.WriteLine("Image saved at:");
                    Console.WriteLine(AppDomain.CurrentDomain.BaseDirectory + @"Capturas\" + titulo);
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Este metodo borra todas las imagenes .jpg dentro de la carpeta 'Capturas', para no llenar el disco de cosas inutiles
        /// Debe ejecutarse antes de realizar pruebas, si estas se van a ver en el html o si se va a enviar por mail, se pueden borrar
        /// despues de esto
        /// </summary>
        static void BorrarImagenes()
        {
            string[] images = Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory + @"Capturas\", "*.jpg");
            for (int i = 0; i < images.Length; i++)
            {
                File.Delete(images[i]);
            }
        }
    }

    /// <summary>
    /// Esta clase controla la secuencia de pasos
    /// Hay 3 tipos, Abrir, Cerrar y Agregar
    /// Abrir provoca que los siguientes pasos tengan un nivel mas de identacion
    /// Cerrar provoca que tanto este como los siguientes pasos tengan un nivel menos de identacion
    /// Agregar no produce cambios en la identacion
    /// 
    /// Idealmente Cada paso Abrir deberia tener un paso Cerrar de contraparte y que este este al final de la accion
    /// </summary>
    public class SecuenciaDePasos
    {
        public List<string> secuencia = new List<string>();
        public int identacion = 0;
        public void AbrirPaso(string mensaje)
        {
            string msg = AgregarIdentacion(mensaje, "+");
            Console.WriteLine(msg);
            secuencia.Add(msg);

            identacion++;
        }

        public void CerrarPaso(string mensaje)
        {
            identacion--;
			if (identacion < 0)
			{
                identacion = 0;
			}
            string msg = AgregarIdentacion(mensaje, "-");
            Console.WriteLine(msg);
            secuencia.Add(msg);
        }

        public void AgregarPaso(string mensaje)
        {
            string msg = AgregarIdentacion(mensaje, "*");
            Console.WriteLine(msg);
            secuencia.Add(msg);
        }

        public void Clear()
        {
            secuencia.Clear();
            identacion = 0;
        }

        string AgregarIdentacion(string mensaje, string token)
        {
            string ident = string.Concat(Enumerable.Repeat("----", identacion));
            mensaje = ident + token + mensaje;
            return mensaje;
        }
    }

    /// <summary>
    /// Clase que contiene informacion relevante al resultado de una prueba
    /// </summary>
    public class ResultadoPrueba
    {
        public enum Resultado { Aprobada, Fallada, Alerta}
        public string nombre_prueba = "";
        public string descripcion = "";

        public Resultado resultado = Resultado.Aprobada;
        public string duracion = "";
        public string detalles_salida = "";
        public string imagen_prueba = "";

        public List<ResultadoIndividual> resultados = new List<ResultadoIndividual>();


        public ResultadoPrueba(string nombre_prueba, string descripcion)
        {
            this.nombre_prueba = nombre_prueba;
            this.descripcion = descripcion;
            this.resultados = new List<ResultadoIndividual>();
        }
        public ResultadoPrueba(Resultado aprobada, string nombre_prueba, string descripcion, string duracion, string detalles_salida)
        {
            this.resultado = aprobada;
            this.nombre_prueba = nombre_prueba;
            this.descripcion = descripcion;
            this.duracion = duracion;
            this.detalles_salida = detalles_salida;
            this.resultados = new List<ResultadoIndividual>();

        }
        public void AgregarResultado(Resultado resultado, string duracion, string detalles_salida, string imagen_prueba)
		{
            resultados.Add(new ResultadoIndividual
            {
                resultado = resultado,
                duracion = duracion,
                detalles_salida = detalles_salida,
                imagen_prueba = imagen_prueba
            });
		}

        public class ResultadoIndividual {
            public Resultado resultado = Resultado.Aprobada;
            public string duracion = "";
            public string detalles_salida = "";
            public string imagen_prueba = "";
        }
    }

    /// <summary>
    /// Excepcion Utilizada para cuando en algun punto de la prueba esta deberia fallar
    /// </summary>
    [Serializable]
    public class PruebaFalladaException : Exception
    {
        public PruebaFalladaException() : base() { }
        public PruebaFalladaException(string message) : base(message) { }
        public PruebaFalladaException(string message, Exception inner) : base(message, inner) { }

        // A constructor is needed for serialization when an
        // exception propagates from a remoting server to the client.
        protected PruebaFalladaException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }

    /// <summary>
    /// Clase para relacionar un nombre con una referencia al metodo donde inicia una prueba
    /// </summary>
    public class Prueba
    {
        public string nombre_prueba;
        public string descripcion;
        public Metodo metodo_prueba;

        public Prueba(string nombre_prueba, Metodo metodo_prueba)
        {
            this.nombre_prueba = nombre_prueba;
            this.metodo_prueba = metodo_prueba;
        }
        public Prueba(string nombre_prueba, string descripcion, Metodo metodo_prueba)
        {
            this.nombre_prueba = nombre_prueba;
            this.descripcion = descripcion;
            this.metodo_prueba = metodo_prueba;
        }

        public delegate void Metodo();
    }
}
