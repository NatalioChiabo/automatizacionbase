﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AutomatizacionBase
{
    public class MouseOperations
    {
        [Flags]
        public enum MouseEventFlags
        {
            LeftDown = 0x00000002,
            LeftUp = 0x00000004,
            MiddleDown = 0x00000020,
            MiddleUp = 0x00000040,
            Move = 0x00000001,
            Absolute = 0x00008000,
            RightDown = 0x00000008,
            RightUp = 0x00000010
        }
        [DllImport("user32.dll")]
        private static extern bool SetProcessDPIAware();

        [DllImport("user32.dll", EntryPoint = "SetCursorPos")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool SetCursorPos(int x, int y);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool GetCursorPos(out MousePoint lpMousePoint);

        [DllImport("user32.dll")]
        private static extern void mouse_event(int dwFlags, int dx, int dy, int dwData, int dwExtraInfo);

        [DllImport("user32.dll")]
        public static extern bool GetWindowRect(IntPtr hwnd, ref Rectangle rectangle);

        [DllImport("user32.dll")]
        static extern bool ClientToScreen(IntPtr hWnd, ref Point lpPoint);

        [DllImport("user32.dll")]
        static extern bool ScreenToClient(IntPtr hWnd, ref Point lpPoint);

        [DllImport("user32.dll")]
        static extern bool GetClientRect(IntPtr hWnd, out RECT lpRect);

        private const int SW_SHOWNORMAL = 1;

        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        private static extern bool ShowWindow(IntPtr hwnd, int nCmdShow);

        [DllImport("user32.dll", SetLastError = true)]
        private static extern bool SetForegroundWindow(IntPtr hwnd);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        static extern int GetWindowText(IntPtr hWnd, StringBuilder lpString, int nMaxCount);

        public static void SetForeground(Process p)
        {
            ShowWindow(p.MainWindowHandle, SW_SHOWNORMAL);
            SetForegroundWindow(p.MainWindowHandle);
        }

        public static void SetCursorPosition(int x, int y)
        {
            SetCursorPos(x, y);
        }

        public static void SetCursorPosition(MousePoint point)
        {
            SetCursorPos(point.X, point.Y);
        }

        public static MousePoint GetCursorPosition()
        {
            MousePoint currentMousePoint;
            var gotPoint = GetCursorPos(out currentMousePoint);
            if (!gotPoint) { currentMousePoint = new MousePoint(0, 0); }
            return currentMousePoint;
        }

        public static void MouseEvent(MouseEventFlags value)
        {
            MousePoint position = GetCursorPosition();

            mouse_event((int)value, position.X, position.Y, 0, 0);
        }

        public static void MouseEvent(MouseEventFlags value, MousePoint position)
        {
            mouse_event((int)value, position.X, position.Y, 0, 0);
        }

        public static void Click()
        {
            MouseEvent(MouseEventFlags.LeftDown);
            Thread.Sleep(50);
            MouseEvent(MouseEventFlags.LeftUp);
        }

        public static void Click(MousePoint position)
        {
            SetCursorPosition(position);
            Thread.Sleep(50);
            Click();
        }

        public static void Click(MousePoint position, Rectangle rect)
        {
            position = new MousePoint(position.X + rect.X, position.Y + rect.Y);

            Click(position);
        }

        public static void DouleClick()
        {
            Click();
            Thread.Sleep(50);
            Click();
        }

        public static void DouleClick(MousePoint position)
        {
            Click(position);
            Thread.Sleep(50);
            Click(position);
        }

        public static void RClick()
        {
            MouseEvent(MouseEventFlags.RightDown);
            Thread.Sleep(50);
            MouseEvent(MouseEventFlags.RightUp);
        }

        public static void RClick(MousePoint position)
        {
            MouseEvent(MouseEventFlags.RightDown, position);
            Thread.Sleep(50);
            MouseEvent(MouseEventFlags.RightUp, position);
        }

        public static void ClickDrag(MousePoint to)
        {
            MouseEvent(MouseEventFlags.LeftDown);
            Thread.Sleep(25);
            SetCursorPosition(to);
            Thread.Sleep(25);
            MouseEvent(MouseEventFlags.LeftUp);
        }
        public static void ClickDrag(MousePoint from, MousePoint to)
        {
            MouseEvent(MouseEventFlags.LeftDown, from);
            Thread.Sleep(25);
            SetCursorPosition(to);
            Thread.Sleep(25);
            MouseEvent(MouseEventFlags.LeftUp, from);
        }

        public static Rectangle GetWindowRectangle(string processName)
        {
            Rectangle rect = new Rectangle();
            Process[] p = Process.GetProcessesByName(processName);
            if (p.Length == 0)
            {
                Console.WriteLine("Failed");
                return rect;
            }
            GetWindowRect(p[0].MainWindowHandle, ref rect);
            return rect;
        }

        public static Rectangle GetWindowRectangle(int pid)
        {
            SetProcessDPIAware();
            Rectangle rect = new Rectangle();
            Process p = Process.GetProcessById(pid);
            GetWindowRect(p.MainWindowHandle, ref rect);
            rect.Width = rect.Width - rect.X;
            rect.Height = rect.Height - rect.Y;
            return rect;
        }

        public static Rectangle GetClientRectangle(int pid)
        {
            RECT rect = new RECT();
            Process p = Process.GetProcessById(pid);
            GetClientRect(p.MainWindowHandle, out rect);
            Point xy = new Point(rect.X, rect.Y);
            ClientToScreen(p.MainWindowHandle, ref xy);
            Point wh = new Point(rect.Width, rect.Height);
            ClientToScreen(p.MainWindowHandle, ref wh);
            Rectangle r = new Rectangle(xy.X,xy.Y,wh.X,wh.Y);
            return r;
        }

        public static Rectangle GetWindowRectangle(Process process)
        {
            Rectangle rect = new Rectangle();
            GetWindowRect(process.MainWindowHandle, ref rect);
            return rect;
        }

        public static string GetWindowTitle(IntPtr pointer)
        {
            try
            {
                StringBuilder title = new StringBuilder(100, 100);
                GetWindowText(pointer, title, 100);
                return title.ToString();
            }
            catch (Exception)
            {
                return "Error";
            }
        }

        public delegate bool Win32Callback(IntPtr hwnd, IntPtr lParam);

        [DllImport("user32.dll")]
        public static extern uint GetWindowThreadProcessId(IntPtr hWnd, out uint lpdwProcessId);

        [DllImport("user32.Dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool EnumChildWindows(IntPtr parentHandle, Win32Callback callback, IntPtr lParam);

        public static List<IntPtr> GetRootWindowsOfProcess(int pid)
        {
            List<IntPtr> rootWindows = GetChildWindows(IntPtr.Zero);
            List<IntPtr> dsProcRootWindows = new List<IntPtr>();
            foreach (IntPtr hWnd in rootWindows)
            {
                uint lpdwProcessId;
                GetWindowThreadProcessId(hWnd, out lpdwProcessId);
                if (lpdwProcessId == pid)
                    dsProcRootWindows.Add(hWnd);
            }
            return dsProcRootWindows;
        }

        public static List<IntPtr> GetChildWindows(IntPtr parent)
        {
            List<IntPtr> result = new List<IntPtr>();
            GCHandle listHandle = GCHandle.Alloc(result);
            try
            {
                Win32Callback childProc = new Win32Callback(EnumWindow);
                EnumChildWindows(parent, childProc, GCHandle.ToIntPtr(listHandle));
            }
            finally
            {
                if (listHandle.IsAllocated)
                    listHandle.Free();
            }
            return result;
        }

        public static bool EnumWindow(IntPtr handle, IntPtr pointer)
        {
            GCHandle gch = GCHandle.FromIntPtr(pointer);
            List<IntPtr> list = gch.Target as List<IntPtr>;
            if (list == null)
            {
                throw new InvalidCastException("GCHandle Target could not be cast as List<IntPtr>");
            }
            list.Add(handle);
            //  You can modify this to check to see if you want to cancel the operation, then return a null here
            return true;
        }
    }
    [StructLayout(LayoutKind.Sequential)]
    public struct MousePoint
    {
        public int X;
        public int Y;

        public MousePoint(int x, int y)
        {
            X = x;
            Y = y;
        }
    }
    [StructLayout(LayoutKind.Sequential)]
    public struct RECT
    {
        public int Left, Top, Right, Bottom;

        public RECT(int left, int top, int right, int bottom)
        {
            Left = left;
            Top = top;
            Right = right;
            Bottom = bottom;
        }

        public RECT(System.Drawing.Rectangle r) : this(r.Left, r.Top, r.Right, r.Bottom) { }

        public int X
        {
            get { return Left; }
            set { Right -= (Left - value); Left = value; }
        }

        public int Y
        {
            get { return Top; }
            set { Bottom -= (Top - value); Top = value; }
        }

        public int Height
        {
            get { return Bottom - Top; }
            set { Bottom = value + Top; }
        }

        public int Width
        {
            get { return Right - Left; }
            set { Right = value + Left; }
        }

        public System.Drawing.Point Location
        {
            get { return new System.Drawing.Point(Left, Top); }
            set { X = value.X; Y = value.Y; }
        }

        public System.Drawing.Size Size
        {
            get { return new System.Drawing.Size(Width, Height); }
            set { Width = value.Width; Height = value.Height; }
        }

        public static implicit operator System.Drawing.Rectangle(RECT r)
        {
            return new System.Drawing.Rectangle(r.Left, r.Top, r.Width, r.Height);
        }

        public static implicit operator RECT(System.Drawing.Rectangle r)
        {
            return new RECT(r);
        }

        public static bool operator ==(RECT r1, RECT r2)
        {
            return r1.Equals(r2);
        }

        public static bool operator !=(RECT r1, RECT r2)
        {
            return !r1.Equals(r2);
        }

        public bool Equals(RECT r)
        {
            return r.Left == Left && r.Top == Top && r.Right == Right && r.Bottom == Bottom;
        }

        public override bool Equals(object obj)
        {
            if (obj is RECT)
                return Equals((RECT)obj);
            else if (obj is System.Drawing.Rectangle)
                return Equals(new RECT((System.Drawing.Rectangle)obj));
            return false;
        }

        public override int GetHashCode()
        {
            return ((System.Drawing.Rectangle)this).GetHashCode();
        }

        public override string ToString()
        {
            return string.Format(System.Globalization.CultureInfo.CurrentCulture, "{{Left={0},Top={1},Right={2},Bottom={3}}}", Left, Top, Right, Bottom);
        }
    }
}
