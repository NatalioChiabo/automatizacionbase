﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;
using System;
using System.Drawing;
using System.Threading;

namespace AutomatizacionBase
{
    public static class Driver
    {

        public static string url = "https://www.google.com/";

        [ThreadStatic]
        public static IWebDriver driver;

        public static AlSalir alTerminarLaPrueba;

        public static Size window_size = new Size(0,0);

        public static string directorio_descarga = AppDomain.CurrentDomain.BaseDirectory + "Descargas";

        public static void IniciarChromeDriver()
        {
            ReportadorDePruebas.secuenciaDePasos.AbrirPaso("Inicializar ChromeDriver");

            //Todo esto es una configuracon para permitir descargar archivos sin que te salte un mensaje
            ChromeOptions chrome_options = new ChromeOptions();
            chrome_options.AddArgument("--safebrowsing-disable-download-protection");
            chrome_options.AddArgument("safebrowsing-disable-extension-blacklist");
            chrome_options.AddArgument("--disable-web-security");
            chrome_options.AddArgument("--ignore-certificate-errors");
            chrome_options.AddArgument("--no-sandbox");
            chrome_options.AddUserProfilePreference("profile.default_content_setting_values.automatic_downloads", 1);
            chrome_options.AddUserProfilePreference("profile.default_content_settings.popups", 0);
            chrome_options.AddUserProfilePreference("disable-popup-blocking", "true");
            chrome_options.AddUserProfilePreference("download.prompt_for_download", false);
            chrome_options.AddUserProfilePreference("safebrowsing.enabled", false);
            chrome_options.AddUserProfilePreference("download_restrictions", "0");
            chrome_options.AddUserProfilePreference("download.default_directory", directorio_descarga);
            //Esta opcion sirve para dejarle saber a chrome que tipo de archivos permitir
            chrome_options.AddUserProfilePreference("download.extensions_to_open", "application/x-java-jnlp-file");

            ChromeDriver cd = new ChromeDriver(chrome_options);
            cd.Manage().Window.Maximize();
            cd.Navigate().GoToUrl(url);

            driver = cd;

            driver.Manage().Cookies.DeleteAllCookies();

            ReportadorDePruebas.secuenciaDePasos.CerrarPaso("ChromeDriver Inicializado");
        }

        public static void IniciarFireFoxDriver()
        {
            ReportadorDePruebas.secuenciaDePasos.AbrirPaso("Inicializar FireFoxDriver");

            FirefoxProfile profile = new FirefoxProfile();

            //Todo esto es una configuracon para permitir descargar archivos sin que te salte un mensaje
            profile.SetPreference("browser.download.folderList", 2);
            profile.SetPreference("browser.download.dir", directorio_descarga);
            profile.SetPreference("browser.safebrowsing.downloads.enabled", false);
            profile.SetPreference("browser.download.manager.focusWhenStarting", false);
            profile.SetPreference("browser.download.useDownloadDir", true);
            profile.SetPreference("browser.helperApps.alwaysAsk.force", false);
            profile.SetPreference("browser.download.manager.alertOnEXEOpen", false);
            profile.SetPreference("browser.download.manager.closeWhenDone", true);
            profile.SetPreference("browser.download.manager.showAlertOnComplete", false);
            profile.SetPreference("browser.download.manager.useWindow", false);
            //Esta opcion sirve para dejarle saber a firefox que tipo de archivos permitir
            profile.SetPreference("browser.helperApps.neverAsk.saveToDisk", "application/octet-stream,text/xml,application/x-ms-application, application/x-java-jnlp-file");

            FirefoxOptions options = new FirefoxOptions();
            options.Profile = profile;

            FirefoxDriver fd = new FirefoxDriver(options);
            fd.Manage().Window.Maximize();
            window_size = fd.Manage().Window.Size;


            driver = fd;

            driver.Navigate().GoToUrl(url);

            ReportadorDePruebas.secuenciaDePasos.CerrarPaso("FireFoxDriver Inicializado");
        }

        public static void IniciarEdgeDriver()
        {
            ReportadorDePruebas.secuenciaDePasos.AbrirPaso("Inicializar EdgeDriver");

            EdgeDriver ed = new EdgeDriver();
            ed.Manage().Window.Maximize();
            window_size = ed.Manage().Window.Size;
            ed.Navigate().GoToUrl(url);

            driver = ed;

            ReportadorDePruebas.secuenciaDePasos.CerrarPaso("EdgeDriver Inicializado");
        }

        public static void IniciarIEDriver()
        {
            ReportadorDePruebas.secuenciaDePasos.AbrirPaso("Inicializar InternetExplorerDriver");

            InternetExplorerDriverService service = InternetExplorerDriverService.CreateDefaultService();
            InternetExplorerOptions options = new InternetExplorerOptions();
            options.PageLoadStrategy = PageLoadStrategy.Eager;

            options.EnsureCleanSession = true;
            options.IntroduceInstabilityByIgnoringProtectedModeSettings = true;

            InternetExplorerDriver ied = new InternetExplorerDriver(service, options, TimeSpan.FromSeconds(20));

            ied.Manage().Window.Maximize();
            window_size = ied.Manage().Window.Size;
            ied.Navigate().GoToUrl(url);

            driver = ied;

            Thread.Sleep(Utils.longSleep);

            ReportadorDePruebas.secuenciaDePasos.CerrarPaso("InternetExplorerDriver Inicializado");
        }

        public static void CerrarWebDriver()
        {
            driver?.Quit();

            alTerminarLaPrueba?.Invoke();
            alTerminarLaPrueba = delegate { };
        }

        public static void AlTerminarPrueba()
		{
            alTerminarLaPrueba?.Invoke();
            alTerminarLaPrueba = delegate { };
        }

        public delegate void AlSalir();

        /// <summary>
        /// Esta funcion debe ser llamada unaves firefoxdriver esta inicializado, basicamente instalara de forma manual la extension.
        /// Utiliza comandos de mouse, asi que si la vas a usar no toques el mouse mientras se ejecuta.
        /// </summary>
        /// <param name="extension_link"></param>
        public static void InstallExtensionFirefox(string extension_link)
        {
            driver.Navigate().GoToUrl(extension_link);
            Thread.Sleep(Utils.longSleep);

            Utils.Click(By.XPath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/section[1]/div[1]/header[1]/div[4]/div[1]/div[1]/a[1]"));
            Thread.Sleep(Utils.extraLongSleep);

            MouseOperations.Click(new MousePoint(600, 200));
            Thread.Sleep(Utils.extraLongSleep);

            Thread.Sleep(Utils.extraLongSleep);
            MouseOperations.Click(new MousePoint(window_size.Width - 57, 210));
            Thread.Sleep(Utils.extraLongSleep);

            Thread.Sleep(Utils.longSleep);
        }

        public static bool AcceptAlert()
		{
			try
			{
                driver?.SwitchTo()?.Alert()?.Accept();
                driver?.SwitchTo()?.DefaultContent();
                Thread.Sleep(Utils.shortSleep);
                return true;
            }
			catch
			{
                return false;
			}
        }
    }
}
