﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace AutomatizacionBase
{
    public static class SQLHook
    {
        public static string tabla_algo = "Catalogo.dbo.Tabla";

        public static string userID = "USUARIO";
        public static string password = "CONTRASEÑA";
        public static string dataSource = "IP_DE_TU_SERVIDOR";
        public static string InitialCatalog = "CATALOGO";
        public static string ConnectionString
        {
            get
            {
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder
                {
                    UserID = userID,
                    Password = password,
                    Authentication = SqlAuthenticationMethod.SqlPassword,
                    DataSource = dataSource,
                    TrustServerCertificate = true,
                    InitialCatalog = InitialCatalog,
                    Encrypt = false
                };
                return builder.ConnectionString;
            }
        }

        /// <summary>
        /// Este metodo envia un comando SQL al servidor SQL cargado al inicio
        /// </summary>
        public static void Set(string sqlCmd)
        {
            using (SqlConnection conexion = new SqlConnection(ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(sqlCmd, conexion))
                {
                    conexion.Open();
                    try
                    {
                        int filasAfectadas = command.ExecuteNonQuery();
                        string msg = "Filas afectadas: " + filasAfectadas.ToString();
                        ReportadorDePruebas.secuenciaDePasos?.AgregarPaso(msg);
                        Console.WriteLine(msg);
                    }
                    catch (Exception ex1)
                    {
                        ReportadorDePruebas.secuenciaDePasos?.AgregarPaso(ex1.Message);
                        Console.WriteLine(ex1.Message);
                    }
                    conexion.Close();
                }
            }
        }

        public static void RunStoreProcedure(string storeProcedureName, SqlParameter[] parametros)
        {
            using (SqlConnection conexion = new SqlConnection(ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(storeProcedureName, conexion))
                {
                    conexion.Open();
                    try
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        foreach (SqlParameter item in parametros)
                        {
                            command.Parameters.Add(item.ParameterName, item.SqlDbType).Value = item.Value;
                        }
                        int filasAfectadas = command.ExecuteNonQuery();
                        string msg = "Filas afectadas: " + filasAfectadas.ToString();
                        ReportadorDePruebas.secuenciaDePasos?.AgregarPaso(msg);
                        Console.WriteLine(msg);
                    }
                    catch (Exception ex1)
                    {
                        ReportadorDePruebas.secuenciaDePasos?.AgregarPaso(ex1.Message);
                        Console.WriteLine(ex1.Message);
                    }
                    conexion.Close();
                }
            }
        }

        public static Table RunStoreProcedureGet(string storeProcedureName, SqlParameter[] parametros)
        {
            List<List<string>> entries = new List<List<string>>();
            List<string> columnNames = new List<string>();

            using (SqlConnection conexion = new SqlConnection(ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(storeProcedureName, conexion))
                {
                    conexion.Open();
                    try
                    {
                        command.CommandType = CommandType.StoredProcedure;

                        foreach (SqlParameter item in parametros)
                        {
                            command.Parameters.Add(item.ParameterName, item.SqlDbType).Value = item.Value;
                        }
                        using (SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection))
                        {
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                columnNames.Add(reader.GetName(i));
                            }

                            while (reader.Read())
                            {
                                entries.Add(new List<string>());
                                for (int i = 0; i < reader.FieldCount; i++)
                                {
                                    string value = reader[i].ToString();//Valor
                                    entries[entries.Count - 1].Add(value);
                                }
                            }
                            reader.Close();
                        }
                    }
                    catch (Exception ex1)
                    {
                        ReportadorDePruebas.secuenciaDePasos?.AgregarPaso(ex1.Message);
                        Console.WriteLine(ex1.Message);
                    }
                    conexion.Close();
                }
            }
            return new Table(columnNames, entries);
        }

        /// <summary>
        /// Este metodo envia un comando SQL al servidor SQL cargado al inicio
        /// Este espera que el comando resulte en una tabla y la retorna
        /// </summary>
        public static Table Get(string sqlCmd)
        {
            List<List<string>> entries = new List<List<string>>();
            List<string> columnNames = new List<string>();

            using (SqlConnection conexion = new SqlConnection(ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(sqlCmd, conexion))
                {
                    conexion.Open();
                    using (SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection))
                    {
                        for (int i = 0; i < reader.FieldCount; i++)
                        {
                            columnNames.Add(reader.GetName(i));
                        }
                        while (reader.Read())
                        {
                            entries.Add(new List<string>());
                            for (int i = 0; i < reader.FieldCount; i++)
                            {
                                string value = reader[i].ToString();//Valor
                                entries[entries.Count - 1].Add(value);
                            }
                        }
                        reader.Close();
                    }
                    conexion.Close();
                }
            }
            return new Table(columnNames, entries);
        }

        public static string Delete(string tabla, string condicion)
        {
            return $"DELETE FROM {tabla}  WHERE {condicion}";
        }

        public static string Update(string tabla, string valueMatch, string condicion)
        {
            return $"UPDATE {tabla} SET {valueMatch}  WHERE {condicion}";
        }

        public static string Select(string tabla, string columnas, string condicion)
        {
            return $"SELECT {columnas} FROM {tabla}  WHERE {condicion}";
        }

        public static string InSelect(string tabla, string columna, string condicion)
        {
            return $"IN ({Select(tabla, columna, condicion)})";
        }
        
        public static string Like(string variable, string valor)
        {
            return $"{variable} LIKE '{valor}'";
        }

        public static string Igual(string variable, string valor)
        {
            return $"{variable} = '{valor}'";
        }
    }

    public class CommandBuilder
    {
        List<CommandNode> sequence = new List<CommandNode>();
        public string Finalizar()
		{
            StringBuilder sb = new StringBuilder();
			foreach (var item in sequence)
			{
                sb.Append(item.Parse() + ";");
			}
            return "";
		}

        public CommandBuilder Delete(string table, ConditionNode condition)
        {
            sequence.Add(new DeleteNode(table, condition));
            return this;
        }

        public CommandBuilder Update(string table, string column, string value, ConditionNode condition)
        {
            sequence.Add(new UpdateNode(table, column, value, condition));
            return this;
        }

        public CommandBuilder Select(string table,string columns, ConditionNode condition)
        {
            sequence.Add(new SelectNode(table, columns, condition));
            return this;
        }

        public abstract class CommandNode {
            public abstract string Parse();
        }
        public class DeleteNode : CommandNode
        {
            string table;
            ConditionNode condition;
            public DeleteNode(string table, ConditionNode condition)
            {
                this.table = table;
                this.condition = condition;
            }
            public override string Parse()
            {
                return $"DELETE FROM {table}  WHERE {condition.Parse()}";
            }
        }

        public class UpdateNode : CommandNode
        {
            string table;
            string column;
            string value;
            ConditionNode condition;
            public UpdateNode(string table, string column, string value, ConditionNode condition)
            {
                this.table = table;
                this.column = column;
                this.value = value;
                this.condition = condition;
            }
            public override string Parse()
            {
                return $"UPDATE {table} SET {column}={value}  WHERE {condition.Parse()}";
            }
        }

        public class SelectNode : CommandNode
        {
            string table;
            string columns;
            ConditionNode condition;
            public SelectNode(string table,string columns, ConditionNode condition)
            {
                this.table = table;
                this.columns = columns;
                this.condition = condition;
            }
            public override string Parse()
            {
                return $"SELECT {columns} FROM {table}  WHERE {condition.Parse()}";
            }
        }

        public abstract class ConditionNode : CommandNode{}

        public class EqualNode : ConditionNode
        {
            string column;
            string value;
            public EqualNode(string column, string value)
			{
                this.column = column;
                this.value = value;
			}
            public override string Parse()
            {
                return $"{column}='{value}'";
            }
        }

        public class LikeNode : ConditionNode
        {
            string column;
            string value;
            public LikeNode(string column, string value)
            {
                this.column = column;
                this.value = value;
            }
            public override string Parse()
            {
                return $"{column} LIKE '{value}'";
            }
        }

        public class InSelectNode : ConditionNode
        {
            SelectNode node;
            public InSelectNode(SelectNode node)
            {
                this.node = node;
            }
            public override string Parse()
            {
                return $"IN ({node.Parse()})";
            }
        }

        public class InNode : ConditionNode
        {
            string[] values;
            public InNode(string[] values)
            {
                this.values = values;
            }
            public InNode(int[] values)
            {
                this.values = new string[values.Length];
                for (int i = 0; i < values.Length; i++)
                {
                    this.values[i] = values[i].ToString();
                }
            }

            public InNode(float[] values)
            {
                this.values = new string[values.Length];
                for (int i = 0; i < values.Length; i++)
                {
                    this.values[i] = values[i].ToString();
                }
            }

            public InNode(bool[] values)
            {
                this.values = new string[values.Length];
                for (int i = 0; i < values.Length; i++)
                {
                    this.values[i] = values[i].ToString();
                }
            }
            public InNode(object[] values)
            {
                this.values = new string[values.Length];
                for (int i = 0; i < values.Length; i++)
                {
                    this.values[i] = values[i].ToString();
                }
            }
            public override string Parse()
            {
                return $"IN ({string.Join(", ", values)})";
            }
        }
    }

    /// <summary>
    /// Clase que contiene una lista de nombres de columnas y una tabla de entradas, usada para organizar la respuesta de una query de sql
    /// </summary>
    public class Table
	{
        public List<string> columnNames;
        public List<List<string>> entries;
        public Table()
        {
            this.columnNames = new List<string>();
            this.entries = new List<List<string>>();
        }
        public Table(List<string> columnNames, List<List<string>> entries)
        {
            this.columnNames = columnNames;
            this.entries = entries;
        }
        public Table(List<List<string>> elements)
        {
            this.columnNames = elements[0];
            this.entries = elements.GetRange(1, elements.Count - 1);
        }

        /// <summary>
        /// Obtiene el elemento de la columna
        /// </summary>
        /// <param name="columnName">Nombre de la columna</param>
        /// <param name="entryNumber">Numero de la entrada</param>
        /// <returns>Retorna el elemento especificado si existe, sino retorna nulo, si la tabla no existe retorna nulo</returns>
        public string GetElement(string columnName, int entryNumber)
        {
            int columnIndex = columnNames.IndexOf(columnName);
            if (columnIndex != -1)
            {
                return entries[entryNumber][columnIndex];
            }
            return null;
        }
        /// <summary>
        /// Obtiene el primer elemento de la columna que cumple con el la expresion match
        /// </summary>
        /// <param name="columnName">Nombre de la columna</param>
        /// <param name="match">expresion para comparar todos los elementos de las columnas</param>
        /// <param name="args">argumentos usados por la expresion match aprte de el elemento de la columna en si</param>
        /// <returns>Retorna el elemento especificado si existe, sino retorna nulo, si la tabla no existe retorna nulo</returns>
        public string GetFirstMatch(string columnName, Match match, object[] args)
        {
            int columnIndex = columnNames.IndexOf(columnName);
            if (columnIndex != -1)
            {
                for (int i = 0; i < entries.Count; i++)
                {
                    if (match.Invoke(entries[i][columnIndex], args))
                    {
                        return entries[i][columnIndex];
                    }
                }
            }
            return null;
        }
        /// <summary>
        /// Obtiene el indice del primer elemento de la columna que cumple con el la expresion match
        /// </summary>
        /// <param name="columnName">Nombre de la columna</param>
        /// <param name="match">expresion para comparar todos los elementos de las columnas</param>
        /// <param name="args">argumentos usados por la expresion match aprte de el elemento de la columna en si</param>
        /// <returns>Retorna el indice del elemento especificado si existe, sino retorna -1, si la tabla no existe retorna -1</returns>
        public int GetFirstMatchIndex(string columnName, Match match, object[] args)
        {
            int columnIndex = columnNames.IndexOf(columnName);
            if (columnIndex != -1)
            {
                for (int i = 0; i < entries.Count; i++)
                {
                    if (match.Invoke(entries[i][columnIndex], args))
                    {
                        return i;
                    }
                }
            }
            return -1;
        }
        /// <summary>
        /// Obtiene el indice de la primer fila de la tabla cuyo valor en la columna especificada es igual al valor dado
        /// </summary>
        /// <param name="columnName">Nombre de la columna</param>
        /// <param name="valueMatch">El valor a comparar</param>
        /// <returns>Retorna el indice de la o especificado si existe, sino retorna -1, si la tabla no existe retorna -1</returns>
        public int GetFirstMatchIndex(string columnName, string valueMatch)
        {
            int columnIndex = columnNames.IndexOf(columnName);
            if (columnIndex != -1)
            {
                for (int i = 0; i < entries.Count; i++)
                {
                    if (entries[i][columnIndex] == valueMatch)
                    {
                        return i;
                    }
                }
            }
            return -1;
        }
        /// <summary>
        /// Obtiene una lista de elementos de la columna que cumplen con el la expresion match
        /// </summary>
        /// <param name="columnName">Nombre de la columna</param>
        /// <param name="match">expresion para comparar todos los elementos de las columnas</param>
        /// <param name="args">argumentos usados por la expresion match aprte de el elemento de la columna en si</param>
        /// <returns>Retorna una lista de los elementos especificados</returns>
        public List<string> GetMatches(string columnName, Match match, object[] args)
        {
            List<string> matches = new List<string>();
            int columnIndex = columnNames.IndexOf(columnName);
            if (columnIndex != -1)
            {
                for (int i = 0; i < entries.Count; i++)
                {
                    if (match.Invoke(entries[i][columnIndex], args))
                    {
                        matches.Add(entries[i][columnIndex]);
                    }
                }
            }
            return matches;
        }
        /// <summary>
        /// Obtiene una lista de elementos de la columna
        /// </summary>
        /// <param name="columnName">Nombre de la columna</param>
        /// <returns>Retorna una lista de todos los elementos de la columna</returns>
        public List<string> GetColumn(string columnName)
        {
            int columnIndex = columnNames.IndexOf(columnName);
            if (columnIndex != -1)
            {
                List<string> column = new List<string>();
                for (int i = 0; i < entries.Count; i++)
                {
                    column.Add(entries[i][columnIndex]);
                }
                return column;
            }
            return null;
        }
        /// <summary>
        /// Obtiene una lista que representa la primer fila de la tabla cuyo valor en la columna especificada es igual al valor dado
        /// </summary>
        /// <param name="columnName">Nombre de la columna</param>
        /// <param name="valueMatch">El valor a comparar</param>
        /// <returns>Retorna una lista de todos los elementos de la columna</returns>
        public List<string> GetFirstEntry(string columnName, string valueMatch)
        {
            int columnIndex = columnNames.IndexOf(columnName);
            if (columnIndex != -1)
            {
                for (int i = 0; i < entries.Count; i++)
                {
                    if (entries[i][columnIndex] == valueMatch)
                    {
                        return entries[i];
                    }
                }
            }
            return null;
        }
        /// <summary>
        /// Obtiene una lista que representa la primer fila de la tabla cuyo valor en la columna especificada es igual al valor dado
        /// </summary>
        /// <param name="columnName">Nombre de la columna</param>
        /// <param name="match">expresion para comparar todos los elementos de las columnas</param>
        /// <param name="args">argumentos usados por la expresion match aprte de el elemento de la columna en si</param>
        /// <returns>Retorna una lista de todos los elementos de la columna</returns>
        public List<string> GetFirstEntry(string columnName, Match match, object[] args)
        {
            int columnIndex = columnNames.IndexOf(columnName);
            if (columnIndex != -1)
            {
                for (int i = 0; i < entries.Count; i++)
                {
                    if (match.Invoke(entries[i][columnIndex], args))
                    {
                        return entries[i];
                    }
                }
            }
            return null;
        }
        /// <summary>
        /// Obtiene una lista de listas con todas las entradas cuyo valor en la columna especificada es igual al valor dado 
        /// </summary>
        /// <param name="columnName"Nombre de la columna></param>
        /// <param name="valueMatch">El valor a comparar</param>
        /// <returns>Retorna una lista de listas que representan las entradas</returns>
        public List<List<string>> GetEntries(string columnName, string valueMatch)
        {
            int columnIndex = columnNames.IndexOf(columnName);
            if (columnIndex != -1)
            {
                List<List<string>> allEntries = new List<List<string>>();
                for (int i = 0; i < entries.Count; i++)
                {
                    if (entries[i][columnIndex] == valueMatch)
                    {
                        allEntries.Add(entries[i]);
                    }
                }
                return allEntries;
            }
            return null;
        }
        /// <summary>
        /// Obtiene una lista de listas con todas las entradas cuyo valor en la columna especificada es igual al valor dado 
        /// </summary>
        /// <param name="columnName"Nombre de la columna></param>
        /// <param name="match">expresion para comparar todos los elementos de las columnas</param>
        /// <param name="args">argumentos usados por la expresion match aprte de el elemento de la columna en si</param>
        /// <returns>Retorna una lista de listas que representan las entradas</returns>
        public List<List<string>> GetEntries(string columnName, Match match, object[] args)
        {
            int columnIndex = columnNames.IndexOf(columnName);
            if (columnIndex != -1)
            {
                List<List<string>> allEntries = new List<List<string>>();
                for (int i = 0; i < entries.Count; i++)
                {
                    if (match.Invoke(entries[i][columnIndex], args))
                    {
                        allEntries.Add(entries[i]);
                    }
                }
                return allEntries;
            }
            return null;
        }

        /// <summary>
        /// Delegate que describe como estructurar las funciones para el uso en las funciones Get de esta clase 
        /// </summary>
        /// <param name="tableEntry">El valor de la tabla que estarias comparando</param>
        /// <param name="args">Los argumentos que le pasarias a la funcion</param>
        /// <returns>El valor de la comparacion</returns>
        public delegate bool Match(string tableEntry, object[] args);

        public static class Generics
		{
            /// <summary>
            /// Funcion que sigue el patron del delegate Match para comparar ints, args deberia tener 1 valor int
            /// </summary>
            public static bool IntEntryGreaterThan(string tableEntry, object[] args)
            {
                if (args.Length != 1) return false;
                if (int.TryParse(tableEntry, out int te) && args[0] is int)
                {
                    return te > (int)args[0];
                }
                return false;
            }
            /// <summary>
            /// Funcion que sigue el patron del delegate Match para comparar ints, args deberia tener 1 valor int
            /// </summary>
            public static bool IntEntryLessThan(string tableEntry, object[] args)
            {
                if (args.Length != 1) return false;
                if (int.TryParse(tableEntry, out int te) && args[0] is int)
                {
                    return te < (int)args[0];
                }
                return false;
            }
            /// <summary>
            /// Funcion que sigue el patron del delegate Match para comparar ints, args deberia tener 1 valor int
            /// </summary>
            public static bool IntEntryGreaterThanEquals(string tableEntry, object[] args)
            {
                if (args.Length != 1) return false;
                if (int.TryParse(tableEntry, out int te) && args[0] is int)
                {
                    return te >= (int)args[0];
                }
                return false;
            }
            /// <summary>
            /// Funcion que sigue el patron del delegate Match para comparar ints, args deberia tener 1 valor int
            /// </summary>
            public static bool IntEntryLessThanEquals(string tableEntry, object[] args)
            {
                if (args.Length != 1) return false;
                if (int.TryParse(tableEntry, out int te) && args[0] is int)
                {
                    return te <= (int)args[0];
                }
                return false;
            }
            /// <summary>
            /// Funcion que sigue el patron del delegate Match para comparar ints, args deberia tener 1 valor int
            /// </summary>
            public static bool IntEntryEquals(string tableEntry, object[] args)
            {
                if (args.Length != 1) return false;
                if (int.TryParse(tableEntry, out int te) && args[0] is int)
                {
                    return te == (int)args[0];
                }
                return false;
            }
            /// <summary>
            /// Funcion que sigue el patron del delegate Match para comparar ints, args deberia tener 2 valores int
            /// </summary>
            public static bool IntEntryBetween(string tableEntry, object[] args)
            {
                if (args.Length != 2) return false;
                if (int.TryParse(tableEntry, out int te) && args[0] is int && args[1] is int)
                {
                    return te > (int)args[0] && te < (int)args[1];
                }
                return false;
            }

            /// <summary>
            /// Funcion que sigue el patron del delegate Match para comparar floats, args deberia tener 1 valor float
            /// </summary>
            public static bool FloatEntryGreaterThan(string tableEntry, object[] args)
            {
                if (args.Length != 1) return false;
                if (float.TryParse(tableEntry, out float te) && args[0] is float)
                {
                    return te > (float)args[0];
                }
                return false;
            }
            /// <summary>
            /// Funcion que sigue el patron del delegate Match para comparar floats, args deberia tener 1 valor float
            /// </summary>
            public static bool FloatEntryLessThan(string tableEntry, object[] args)
            {
                if (args.Length != 1) return false;
                if (float.TryParse(tableEntry, out float te) && args[0] is float)
                {
                    return te < (float)args[0];
                }
                return false;
            }
            /// <summary>
            /// Funcion que sigue el patron del delegate Match para comparar floats, args deberia tener 1 valor float
            /// </summary>
            public static bool FloatEntryGreaterThanEquals(string tableEntry, object[] args)
            {
                if (args.Length != 1) return false;
                if (float.TryParse(tableEntry, out float te) && args[0] is float)
                {
                    return te >= (float)args[0];
                }
                return false;
            }
            /// <summary>
            /// Funcion que sigue el patron del delegate Match para comparar floats, args deberia tener 1 valor float
            /// </summary>
            public static bool FloatEntryLessThanEquals(string tableEntry, object[] args)
            {
                if (args.Length != 1) return false;
                if (float.TryParse(tableEntry, out float te) && args[0] is float)
                {
                    return te <= (float)args[0];
                }
                return false;
            }
            /// <summary>
            /// Funcion que sigue el patron del delegate Match para comparar floats, args deberia tener 1 valor float
            /// </summary>
            public static bool FloatEntryEquals(string tableEntry, object[] args)
            {
                if (args.Length != 1) return false;
                if (float.TryParse(tableEntry, out float te) && args[0] is float)
                {
                    return te == (float)args[0];
                }
                return false;
            }
            /// <summary>
            /// Funcion que sigue el patron del delegate Match para comparar floats, args deberia tener 2 valores float
            /// </summary>
            public static bool FloatEntryBetween(string tableEntry, object[] args)
            {
                if (args.Length != 2) return false;
                if (float.TryParse(tableEntry, out float te) && args[0] is float && args[1] is float)
                {
                    return te > (float)args[0] && te < (float)args[1];
                }
                return false;
            }

            /// <summary>
            /// Funcion que sigue el patron del delegate Match para comparar strings, args deberia tener 1 valor string
            /// </summary>
            public static bool StringEquals(string tableEntry, object[] args)
            {
                if (args.Length != 1) return false;
                return tableEntry.Equals((string)args[0]);
            }
            /// <summary>
            /// Funcion que sigue el patron del delegate Match para comparar strings, args deberia tener 1 valor string
            /// </summary>
            public static bool StringContains(string tableEntry, object[] args)
            {
                if (args.Length != 1) return false;
                return tableEntry.Contains((string)args[0]);
            }
            /// <summary>
            /// Funcion que sigue el patron del delegate Match para comparar strings, args deberia tener 1 valor string
            /// </summary>
            public static bool StringStartsWith(string tableEntry, object[] args)
            {
                if (args.Length != 1) return false;
                return tableEntry.StartsWith((string)args[0]);
            }
            /// <summary>
            /// Funcion que sigue el patron del delegate Match para comparar strings, args deberia tener 1 valor string
            /// </summary>
            public static bool StringEndsWith(string tableEntry, object[] args)
            {
                if (args.Length != 1) return false;
                return tableEntry.EndsWith((string)args[0]);
            }
            /// <summary>
            /// Funcion que sigue el patron del delegate Match para comparar strings, args deberia tener valores string
            /// </summary>
            public static bool StringIsIn(string tableEntry, object[] args)
            {
                for (int i = 0; i < args.Length; i++)
                {
                    if (tableEntry.Equals((string)args[0]))
                    {
                        return true;
                    }
                }
                return false;
            }

            /// <summary>
            /// Funcion que sigue el patron del delegate Match, tiene que tener 1 argumento de tipo MatchContainer
            /// </summary>
            public static bool Negate(string tableEntry, object[] args)
            {
                if (args.Length != 1) return false;
                if (!(args[0] is MatchContainer)) return false;
                return !((MatchContainer)args[0]).Invoke(tableEntry);

            }
            /// <summary>
            /// Funcion que sigue el patron del delegate Match, tiene que tener 2 argumentos de tipo MatchContainer
            /// </summary>
            /// <returns>Retorna la operacion And sobre los resultados de los 2 Match</returns>
            public static bool And(string tableEntry, object[] args)
            {
                if (args.Length != 2) return false;
                if (!(args[0] is MatchContainer)) return false;
                if (!(args[1] is MatchContainer)) return false;
                return ((MatchContainer)args[0]).Invoke(tableEntry) && ((MatchContainer)args[1]).Invoke(tableEntry);
            }
            /// <summary>
            /// Funcion que sigue el patron del delegate Match, tiene que tener 2 argumentos de tipo MatchContainer
            /// </summary>
            /// <returns>Retorna la operacion Or sobre los resultados de los 2 Match</returns>
            public static bool Or(string tableEntry, object[] args)
            {
                if (args.Length != 2) return false;
                if (!(args[0] is MatchContainer)) return false;
                if (!(args[1] is MatchContainer)) return false;
                return ((MatchContainer)args[0]).Invoke(tableEntry) || ((MatchContainer)args[1]).Invoke(tableEntry);
            }

            public class MatchContainer
            {
                public Match match;
                public object[] args;
                public MatchContainer(Match match, object[] args)
                {
                    this.match = match;
                    this.args = args;
                }
                public bool Invoke(string tableEntry)
                {
                    return match.Invoke(tableEntry, args);
                }
            }
        }
    }
}
