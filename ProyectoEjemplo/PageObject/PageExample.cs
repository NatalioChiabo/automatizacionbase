﻿using OpenQA.Selenium;

namespace AutomatizacionBase.PageObject
{
	public static class PageExample
	{
		//Es mas conveniente tener el By creado directamente que tener un archivo xml, la idea del archivo xml era que se pueda editar
		//el selector desde fuera del programa, pero dado la forma en la que se automatiza el proceso, esto ya no tiene mucho uso.
		//recomiendo llamar a los By respecto a lo que representan boton_1, input_2, dropdown_3, date_4, etc
		public static By input_email = By.Id("example");
		public static By input_contraseña = By.Id("example");
		public static By boton_login = By.Id("example");
		public static By boton_logout = By.Id("example");
	}
}
