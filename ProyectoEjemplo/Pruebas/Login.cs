﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AutomatizacionBase.PageObject;

namespace AutomatizacionBase.Pruebas
{
	public static class Login
    {
        //Si tienes muchos usuarios, es conveniente acomodarlos en una clase como esta
        public static User usuario1 = new User("email@falso.com", "contraseña");

        public static void LogIn(User user)
        {

            ReportadorDePruebas.secuenciaDePasos.AgregarPaso("Login con usuario " + user.email);

            PageExample.input_email.SendKeys(user.email);
            PageExample.input_contraseña.SendKeys(user.password);

            Thread.Sleep(Utils.shortSleep);

            Utils.Click(PageExample.boton_login);
            Thread.Sleep(Utils.extraLongSleep);
        }

        public static void LogOut()
        {

            ReportadorDePruebas.secuenciaDePasos.AgregarPaso("Logout");
            
            Utils.Click(PageExample.boton_logout);

            Thread.Sleep(Utils.midSleep);
        }
    }

    public struct User
	{
        public string email;
        public string password;
        public User(string email, string password)
		{
            this.email = email;
            this.password = password;
		}
	}
}
