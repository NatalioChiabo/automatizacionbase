﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AutomatizacionBase.PageObject;

namespace AutomatizacionBase.Pruebas
{
	public static class PruebaExample
	{
		public static void Prueba_Example()
		{
			Driver.alTerminarLaPrueba += () =>
			{
				EjemploLimpiezaBaseSQL(Login.usuario1);
			};

			Login.LogIn(Login.usuario1);
			Thread.Sleep(Utils.shortSleep);
		}

		public static void EjemploLimpiezaBaseSQL(User user)
		{
			string tabla = "Ejemplo.dbo.Usuario";
			string cmd = SQLHook.Delete(tabla, $"email={user.email}");
			SQLHook.Set(cmd);
		}
	}
}
