﻿using System;
using System.IO;
using AutomatizacionBase;
using AutomatizacionBase.Pruebas;

namespace ProyectoEjemplo
{
    static class Inicio
    {
        /// <summary>
        /// Carpetas a crear al inicio del programa, se puede especificar subcarpetas
        /// </summary>
        static readonly string[] carpetas_a_crear = { "Capturas", "Descargas" };

        /// <summary>
        /// Las opciones del menu principal
        /// </summary>
        static MenuOption[] options = new MenuOption[]
            {
                new MenuOption("Probar [T]ODO", ProbarTODO,optionalInput: new char[] {'t'}),
                new MenuOption("Probar [I]ndividual", ProbarModulo,optionalInput: new char[] {'i'}),

                new MenuOption("[O]pciones", MenuOpciones,optionalInput:new char[] {'o'}),
                new MenuOption("[S]alir", () => { }, optionalInput: new char[] { 's' },stopMenu:true)
            };

        static void Main(string[] args)
        {
            ReportadorDePruebas.todas_las_pruebas = new Prueba[]
            {
                new Prueba("Prueba de circuito de documento",
                    "Una Prueba de ejemplo",
                    PruebaExample.Prueba_Example),
            };

            Parameters p = ParseArgs(args);
            CreateFolderStructure();

            if (p.url != null)
            {
				Driver.url = p.url;
            }

            if (p.automatic)
            {
                Console.WriteLine("Corriendo TODAS la pruebas...");


                ReportadorDePruebas.CorrerPruebas("Prueba total", ReportadorDePruebas.todas_las_pruebas);
            }
            else
            {

                Console.CursorVisible = false;
                Menu mainMenu = new Menu("", "", options, true);

                mainMenu.DrawMenu();
            }
        }

        static Parameters ParseArgs(string[] args)
        {
            Parameters p = new Parameters();
            for (int i = 0; i < args.Length; i++)
            {
                if (args[i] == "-a")
                {
                    p.automatic = true;
                }
                else if (args[i].Split(':')[0] == "-u")
                {
                    p.url = args[i].Substring(3);
                }

            }
            return p;
        }

        static void ProbarTODO()
        {
            Console.Clear();
            Console.WriteLine("Corriendo TODAS la pruebas...");
            ReportadorDePruebas.CorrerPruebas("Prueba total", ReportadorDePruebas.todas_las_pruebas,open_email:true);
            Console.Clear();
        }


        static void ProbarModulo()
        {
            string circDoc = "PruebaExample";

            MenuOption[] modulos = new MenuOption[ReportadorDePruebas.todas_las_pruebas.Length + 1];
			for (int i = 0; i < ReportadorDePruebas.todas_las_pruebas.Length; i++)
			{
                string name = ReportadorDePruebas.todas_las_pruebas[i].nombre_prueba;
                modulos[i] = new MenuOption(name, () => { ReportadorDePruebas.CorrerPruebas(name, new Prueba[] { ReportadorDePruebas.todas_las_pruebas[i] }, open_email: true); });
			}
            modulos[modulos.Length - 1] = new MenuOption("Volver", () => { }, stopMenu: true);

            Menu menuModulos = new Menu("", "", modulos);

            menuModulos.DrawMenu();
        }


        static void MenuOpciones()
        {
            MenuOption[] opciones = new MenuOption[]
            {
                new MenuOption($"Cantidad de Hilos:  < {ReportadorDePruebas.threadLimit} >", null),
                new MenuOption("Volver", null,stopMenu:true)
            };

            MenuOption opcion = opciones[0];
            opcion.horizontal = derecha =>
            {
                if (derecha)
                {
                    ReportadorDePruebas.threadLimit++;
                }
                else
                {
                    if (ReportadorDePruebas.threadLimit > 1)
                    {
                        ReportadorDePruebas.threadLimit--;
                    }
                }
                opcion.lable = $"Cantidad de Hilos:  < {ReportadorDePruebas.threadLimit} >";
            };

            Menu menuModulos = new Menu("", "", opciones);

            Console.Clear();
            menuModulos.DrawMenu();
            Console.Clear();
        }

        static void CreateFolderStructure()
        {
            for (int i = 0; i < carpetas_a_crear.Length; i++)
            {
                string dir = AppDomain.CurrentDomain.BaseDirectory + carpetas_a_crear[i];
                if (!Directory.Exists(dir))
                {
                    Directory.CreateDirectory(dir);
                }
            }
        }

        class Parameters
        {
            public bool automatic = false;
            public string url = null;
        }
    }
}
